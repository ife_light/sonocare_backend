import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';
import History from '../../../models/History';


export default class  {
    static async getHistories (req, res) {
        try {
            const { type, limit=20, page=1, status, order_by  } = req.query;
            const userId = req.app.get('userId');

            const user = await User.findById(userId).exec();

            const queryObject = {};

            if (type) {
                queryObject.type = type;
            }

            if (status) {
                queryObject.status = status;
            }

            const count = await History.countDocuments({
                [user.type] : userId,
                ...queryObject
            }).exec();

            let pages = Math.ceil(count / limit);
            const toSkip = limit * (page - 1);

            const histories = await History.find({
                ...queryObject
            })
            .populate({ path: 'appointment', select: '_id appointmentDate status service description' })
            .populate({ path: 'partner', select: 'institutionAddress institutionType institutionName _id' })
            .populate({ path: 'patient', select: 'firstName lastName sex _id' })
            .limit(limit)
            .skip(toSkip)
            .exec()

            return successResponse(res, "Histories fetched successfully", histories, {
                page,
                pages
            })
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}