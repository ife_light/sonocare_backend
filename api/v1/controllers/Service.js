import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';

import { emailExist } from '../../../services/emailExist';

import Service from '../../../models/Service';

import createServiceValidation from '../validations/createService';
import updateServiceValidation from '../validations/updateService';


export default class {
    static async listServices (req, res) {
        try {
            const { page=1, limit=20 } = req.query;

            const count = await Service.countDocuments().exec();

            let pages = Math.ceil(count / limit);
            const toSkip = limit * (page - 1);

            const services = await Service.find()
            .limit(limit)
            .skip(toSkip)
            .exec()

            return successResponse(res, "Services retrieved successfully", services, {
                page,
                pages
            })

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async createService (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = createServiceValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { name, description } = req.body;

            const nameExist = await Service.findOne({
                name
            }).exec();

            if (nameExist) {
                return errorResponse(res, 400, "Service with that name already exist");
            }

            const newService = await Service.create({
                name,
                description
            })

            return successResponse(res, "New service created", newService)


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async uploadServiceImage (req, res) {
        try {
            const { serviceId } = req.params;

            const validServiceId = await Service.findById(serviceId).exec();

            if (!validServiceId) {
                return errorResponse(err, 400, "The service with that ID does not exist")
            }

            if (!req.file) {
                return errorResponse(res, 400, "No or invalid image")
            }
            const imageUrl = req.file.secure_url;
            const imageId = req.file.public_id;

            const updateDetails = await Service.findByIdAndUpdate(serviceId, {
                picture: imageUrl
            })

            const service = await Service.findById(serviceId).exec();
            return successResponse(res, "Image updated successfully", service)
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async updateService (req, res) {
        try {
            const { body } = req;

            const { serviceId } = req.params;

            const validService = await Service.findById(serviceId).exec();

            if (!validService) {
                return errorResponse(err, 400, "The service with that ID does not exist")
            }

            //Validation need to be done here
            const errors = updateServiceValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { name, description } = req.body;

            if (name && name != validService.name) {
                const nameExist = await Service.findOne({
                    name
                }).exec();
    
                if (nameExist) {
                    return errorResponse(res, 400, "Service with that name already exist");
                }  
            }

            const updatedServiceDetails = await Service.findByIdAndUpdate(serviceId, {
                name,
                description
            })

            const service = await Service.findById(serviceId).exec();

            return successResponse(res, "Service updated successfully", service);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async deleteService (req, res) {
        try {
            const { serviceId } = req.params;

            const validServiceId = await Service.findById(serviceId).exec();

            if (!validServiceId) {
                return errorResponse(err, 400, "The service with that ID does not exist")
            }

            const deletedServiceDetails = await Service.findByIdAndDelete(serviceId).exec();

            return successResponse(res, "Service deleted successfully");


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}