import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';
import listUsersResponse from '../responses/listUsersResponse'

import User from '../../../models/User';
import Service from '../../../models/Service';
import SubService from '../../../models/SubService';

import updateLocationValidation from '../validations/updateLocation';

import isAuthenticatedService from '../services/isAuthenticated';

export default class Partner {
    static async listPartners(req, res) {
        try {
            const {
                page = 1, limit = 10, latitude, longitude, service, active
            } = req.query;

            const isAuthenticated = isAuthenticatedService(req, res);

            if (!isAuthenticated && req.headers['x-access-token']) {
                return res.status(401).json({
                    auth: false,
                    token: null,
                    message: 'Failed to authenticate token.',
                });
            }

            let options = {
                res,
                page,
                limit,
                service,
                type: 'partner',
                active //could be 'true' or 'false'
            }

            if (isAuthenticated) {
                const user = await User.findById(isAuthenticated).exec();
                if (user && user.isAdmin) {
                    options.user = user.type;
                }
            }

            if (service) {
                const validServiceId = await Service.findById(service).exec();
                const validSubServiceId = await SubService.findById(service).exec();

                if (!validServiceId && !validSubServiceId) {
                    return errorResponse(err, 400, "The service or subservice with that ID does not exist")
                }

                if (validServiceId) {
                    options.serviceType = 'services';
                } else {
                    options.serviceType = 'subServices';
                }
            }

            if (latitude && longitude) {
                const location = {
                    latitude,
                    longitude
                }

                const errors = updateLocationValidation(location);

                if (errors) {
                    return errorResponse(res, 400, errors);
                }

                options = {
                    ...options,
                    location
                }
            } else {
                if (isAuthenticated) {
                    const user = await User.findById(isAuthenticated).exec();
                    if (user) {
                        const check1 = user.location && user.location.coordinates && user.location.coordinates[0];
                        const check2 = user.location && user.location.coordinates && user.location.coordinates[1];
                        if (check1 && check2) {
                            const location = {
                                latitude: user.location.coordinates[1],
                                longitude: user.location.coordinates[0]
                            }
                            options = {
                                ...options,
                                location
                            }
                        }
                    }
                }
            }

            return listUsersResponse(options);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async addService(req, res) {
        try {
            const userId = req.app.get('userId');

            const {
                serviceId
            } = req.params;

            let serviceType;

            const validServiceId = await Service.findById(serviceId).exec();
            const validSubServiceId = await SubService.findById(serviceId).exec();

            if (!validServiceId && !validSubServiceId) {
                return errorResponse(err, 400, "The service or subservice with that ID does not exist")
            }

            if (validServiceId) {
                serviceType = 'services';
            } else {
                serviceType = 'subServices';
            }

            const addToSetField = {
                [serviceType]: serviceId
            }

            const updateDetails = await User.findByIdAndUpdate(userId, {
                $addToSet: addToSetField
            })

            return successResponse(res, "Service added successfully")

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async removeService(req, res) {
        try {
            const userId = req.app.get('userId');

            const {
                serviceId
            } = req.params;

            let serviceType;

            const validServiceId = await Service.findById(serviceId).exec();
            const validSubServiceId = await SubService.findById(serviceId).exec();

            if (!validServiceId && !validSubServiceId) {
                return errorResponse(err, 400, "The service or subservice with that ID does not exist")
            }

            if (validServiceId) {
                serviceType = 'services';
            } else {
                serviceType = 'subServices';
            }

            const removeDetails = await User.findByIdAndUpdate(userId, {
                $pull: {
                    [serviceType]: serviceId
                }
            })

            return successResponse(res, "Service removed successfully")

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async listServices(req, res) {
        try {
            const userId = req.app.get('userId');

            const user = await User.findById(userId)
                .populate({
                    path: 'services',
                    select: 'name description picture _id'
                })
                .exec()

            return successResponse(res, "Partner services fetched successfully", user.services)

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async listSubServices(req, res) {
        try {
            const userId = req.app.get('userId');

            const user = await User.findById(userId)
                .populate({
                    path: 'subServices',
                    select: 'name parent description picture _id service'
                })
                .exec()

            return successResponse(res, "Partner subservices fetched successfully", user.subServices)

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async subscribeToAPartner(req, res) {
        try {
            // Yet to be done

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }


}