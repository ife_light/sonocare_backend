import dotenv from 'dotenv';

import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';
import listUsersResponse from '../responses/listUsersResponse'

import { emailExist } from '../../../services/emailExist';

import createAdminValidation from '../validations/createAdmin';

import User from '../../../models/User';


dotenv.config();


export default class Admin {
    static async createAdmin (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = createAdminValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { email } = body;
            
            //Email Exist Check
            const emailExistCheck = await emailExist(email);
            if (emailExistCheck) {
                return errorResponse(res, 400,  "Email already Exist");
            }


            //Passsword Hashing
            const hashedPassword = User.generateHash(body.password);

            const user = await User.create({
                type: 'admin',
                email,
                isAdmin: true,
                password: hashedPassword
            })

            if (!user) {
                throw new Error("Couldn't create a new Admin")
            }

            return userResponse(res, user._id, false);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async initiateFirstAdmin (req, res) {
        try {
            const email = process.env.ADMIN_EMAIL || 'compuday@gmail.com';
            const password = process.env.ADMIN_PASSWORD || 'Sonocare101.';

            //Email Exist Check
            const emailExistCheck = await emailExist(email);
            if (emailExistCheck) {
                return errorResponse(res, 400,  "Email already Exist");
            }

            //Passsword Hashing
            const hashedPassword = User.generateHash(password);

            const user = await User.create({
                type: 'admin',
                email,
                password: hashedPassword,
                isAdmin: true,
                superAdmin: true
            })

            if (!user) {
                throw new Error("Couldn't create a new Admin")
            }

            return userResponse(res, user._id);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async listAdmins (req, res) {
        try {
            const { page, limit } = req.query;

            return listUsersResponse({res, message: 'Admins retrieved successfully', type: "admin", page, limit })
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }


    static async makeAdmin (req, res) {
        try {
            const { userId } = req.params;

            //Check the validity of the ID
            const user = await User.findOne({
                _id: userId
            }).exec()

            if (!user) {
                return errorResponse(res, 400, "The user id is not valid")
            }

            const updateUser = await User.findByIdAndUpdate(user._id, {
                type: 'admin',
                isAdmin: true
            }).exec()

            return userResponse(res, user._id, false);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async unmakeAdmin (req, res) {
        try {
            const { userId } = req.params;

            //Check the validity of the ID
            const user = await User.findOne({
                _id: userId
            }).exec()

            if (!user) {
                return errorResponse(res, 400, "The user id is not valid")
            }

            const updateUser = await User.findByIdAndUpdate(user._id, {
                type: 'patient',
                isAdmin: false,
                isSuperAdmin: false
            }).exec()

            return userResponse(res, user._id, false);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async deleteAdmin (req, res) {
        try {
            const { userId } = req.params;

            //Check the validity of the ID
            const user = await User.findOne({
                _id: userId
            }).exec()

            if (!user) {
                return errorResponse(res, 400, "The user id is not valid")
            }

            if (!user.isAdmin) {
                return errorResponse(res, 400, "The user id is not an admin")
            }

            const deleteDetails = await User.findByIdAndRemove(user._id).exec();

            return successResponse(res, "Admin deleted successfully")
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async activatePartner (req, res) {
        try {
            const { partnerId } = req.params;

            //Check the validity of the ID
            const user = await User.findOne({
                _id: partnerId,
                type: 'partner'
            }).exec()

            if (!user) {
                return errorResponse(res, 400, "The partner id is not valid")
            }

            const updateDetails = await User.findByIdAndUpdate(partnerId, {
                active: true
            }).exec();

            return successResponse(res, "Partner activated successfully")
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async deactivatePartner (req, res) {
        try {
            const { partnerId } = req.params;

            //Check the validity of the ID
            const user = await User.findOne({
                _id: partnerId,
                type: 'partner'
            }).exec()

            if (!user) {
                return errorResponse(res, 400, "The partner id is not valid")
            }

            const updateDetails = await User.findByIdAndUpdate(partnerId, {
                active: false
            }).exec();

            return successResponse(res, "Partner deactivated successfully")
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
}