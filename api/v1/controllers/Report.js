import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';


import User from '../../../models/User';
import Appointment from '../../../models/Appointment';
import Report from '../../../models/Report';
import File from '../../../models/File';

import createReportValidation from '../validations/createReport';
import updateReportValidation from '../validations/updateReport';

import notificationHandler from './../../../services/notificationHandler';


export default class {
    static async listReports (req, res) {
        try {
            const { page=1, limit=20 } = req.query;

            const userId = req.app.get('userId');

            const user = await User.findById(userId).exec();

            const userType = user.type //Checks wether a Partner or Patient

            const count = await Report.countDocuments({
                [userType] : userId
            }).exec();

            let pages = Math.ceil(count / limit);
            const toSkip = limit * (page - 1);

            const reports = await Report.find({
                [userType] : userId
            })
            .populate('files')
            .populate('appointment', '_id service description status')
            .limit(limit)
            .skip(toSkip)
            .exec()

            return successResponse(res, "Reports retrieved successfully", reports, {
                page,
                pages
            });

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async createReport (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;

            const user = await User.findById(userId).exec();

            if (user.type == "patient") {
                return errorResponse(err, 403, "You are forbidden for creating a report");
            }

            const appointment = await Appointment.findById(appointmentId).exec();
            if (!appointment) {
                return errorResponse(err, 400, "The appointment with that ID does not exist")
            }

            const patientId = appointment.patientId;

            const patient = await User.findById(patientId).exec();

            const { body } = req;

            //Validation need to be done here
            const errors = createReportValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { title, description } = req.body;

            const newReport = await Report.create({
                partner: userId,
                patient: patientId,
                appointment: appointmentId,
                title,
                description,
            });

            if (patient.fcmNotificationId) {
                notificationHandler({
                    partner: user,
                    patient,
                    type: 'report',
                    to: patientId,
                    eventType: 'new-report',
                    data: {
                        appointment_id: appointmentId,
                        report_id: newReport._id,
                        category: 'report',
                        action: "report-created"
                    },
                    fcmNotificationId: patient.fcmNotificationId
                })
            }

            return successResponse(res, "New report created", newReport)


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async updateReport (req, res) {
        try {
            const { body } = req;

            const { reportId } = req.params;
            const userId = req.app.get('userId');

            //Validation need to be done here
            const errors = updateReportValidation(body);
            if (errors) {
                return errorResponse(res, 400, errors);
            }
            

            const report = await Report.findById(reportId).exec();

            if (report.partner != userId) {
                return errorResponse(err, 403, "This user is forbidden to update this report")
            }

            if (!report) {
                return errorResponse(err, 400, "The report with that ID does not exist")
            }

            const updatedReport = await Report.findByIdAndUpdate(reportId, {
                ...body
            }).exec()

            return successResponse(res, "Report updated successfully", updatedReport)


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async viewAReport (req, res) {
        try {
            const userId = req.app.get('userId');
            //const user = await User.findById(userId).exec();

            const { reportId } = req.params;

            const validReportId = await Report.findById(reportId).exec();

            if (!validReportId) {
                return errorResponse(err, 400, "The report with that ID does not exist")
            }

            const theReport = await Report.findOne({
                _id: reportId,
                $or : [
                    { partner: userId },
                    { patient: userId }
                ]
            })
            .populate('files')
            .populate('appointment', '_id service description status')
            .exec()

            return successResponse(res, "Report fetched successfully", theReport);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async viewAReportByAppointment (req, res) {
        try {
            const userId = req.app.get('userId');
            //const user = await User.findById(userId).exec();

            const { appointmentId } = req.params;

            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(err, 400, "The report with that appointment ID does not exist");
            }

            if (appointment.partnerId != userId && appointment.patientId != userId) {
                return errorResponse(res, 403, "You are forbidden to request this report");
            }

            const theReport = await Report.findOne({
                appointment: appointmentId,
            })
            .populate('files')
            .populate('appointment', '_id service description status')
            .exec()

            return successResponse(res, "Report fetched successfully", theReport);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }


    static async addReportFile (req, res) {
        try {
            const { reportId } = req.params;
            const userId = req.app.get('userId');

            const report = await Report.findById(reportId).exec();
            if (userId != report.partner) {
                return errorResponse(res, 403, "You are forbidden to upload report file");
            }

            if (!req.file) {
                return errorResponse(res, 400, "No or invalid file")
            }

            const { fieldname, blob, container, size, url} = req.file;

            const newFile = await File.create({
                azureContainer: container,
                fieldname,
                blob,
                size,
                url
            });

            const updateDetails = await Report.findOneAndUpdate({
                _id: reportId
            }, {
                $push: { files: newFile._id}
            }).exec()


            const file = await File.findById(newFile._id).exec();
            return successResponse(res, "File uploaded successfully", file)
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async removeReportFile (req, res) {
        try {
            const { reportId, fileId } = req.params;

            const userId = req.app.get('userId');

            const report = await Report.findById(reportId).exec();
            if (userId != report.partner) {
                return errorResponse(res, 403, "You are forbidden to remove report file");
            }
            
            const updateDetails = await Report.findOneAndUpdate({
                _id: reportId
            }, {
                $pull: {files: fileId}
            })

            return successResponse(res, "File removed successfully", {});
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async deleteReport (req, res) {
        try {
            const userId = req.app.get('userId');
            const { reportId } = req.params;

            const theReport = await Report.findById(reportId).exec();

            if (theReport.partner != userId) {
                return errorResponse(res, 403, "You are forbidden to deete this report");
            }

            const deletedReportDetails = await Report.findByIdAndDelete(reportId).exec();

            return successResponse(res, "Report deleted successfully");


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}