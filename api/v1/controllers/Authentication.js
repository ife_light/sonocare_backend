import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';

import {emailExist} from '../../../services/emailExist';
import {phoneExist} from '../../../services/phoneExist';
  
import User from '../../../models/User';

import loginValidation from '../validations/login';
import partnerSignupValidation from '../validations/partnerSignup';
import patientSignupValidation from '../validations/patientSignup';
import forgotPasswordValidation from '../validations/forgotPassword'
import passwordCodeConfirmValidation from '../validations/passwordCodeConfirm';

import passwordeCodeMail from '../mails/passwordCode';
import successResponse from '../responses/successResponse';

export default class Authentication {
    static async login (req, res) {
        try {
            const { body } = req;
            //Validation need to be done here
            const errors = loginValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { email, password } = body;

            const user = await User.findOne({
                email
            })
            .exec();

            if (!user) {
                return errorResponse(res, 400, "This Email don't exist")
            }

            if (!user.validPassword(password)) {
                return errorResponse(res, 400, "Password is incorrect");
            }

            return userResponse(res, user._id);

        } catch (err) {
            console.log("an error here");
            console.log(err);
            return response505(res);
        }
    }

    static async signupPatient (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = patientSignupValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { email } = body;
            
            //Email Exist Check
            const emailExistCheck = await emailExist(email);
            if (emailExistCheck) {
                return errorResponse(res, 400,  "Email already Exist");
            }

            const {
                first_name : firstName,
                last_name : lastName,
                address,
                phone,
                date_of_birth : dateOfBirth,
                sex = "male",
                income_range_max: incomeRangeMax ="",
                income_range_min : incomeRangeMin = "",
                income_range_currency : incomeRangeCurrency = "naira"
            } = body;

            //Phone Exist Check
            const phoneExistCheck = await phoneExist(phone);
            if (phoneExistCheck) {
                return errorResponse(res, 400,  "Phone already in use");
            }

            //Passsword Hashing
            const passwordHashing = {password : User.generateHash(body.password)}

            const user = await User.create({
                type: 'patient',
                firstName,
                lastName,
                address,
                phone,
                email,
                dateOfBirth,
                sex,
                incomeRange : {
                    max : incomeRangeMax,
                    min : incomeRangeMin,
                    currency : incomeRangeCurrency
                },
                ...passwordHashing
            })

            if (!user) {
                throw new Error("Couldn't create a new User")
            }

            return userResponse(res, user._id);
            
        } catch (err) {
            console.error(err)
            return response505(res);
        }
    }

    static async signupPartner (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = partnerSignupValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }
            

            const { email } = body;

            const {
                institution_name : institutionName,
                institution_type : institutionType = "",
                institution_address : institutionAddress,
                phone,
            } = body;
            
            //Email Exist Check
            const emailExistCheck = await emailExist(email);
            if (emailExistCheck) {
                return errorResponse(res, 400,  "Email already Exist");
            }

            //Phone Exist Check
            const phoneExistCheck = await phoneExist(phone);
            if (phoneExistCheck) {
                return errorResponse(res, 400,  "Phone already in use");
            }

            //Passsword Hashing
            const passwordHashing = {password : User.generateHash(body.password)}

            const user = await User.create({
                type: 'partner',
                institutionName,
                institutionType,
                institutionAddress,
                phone,
                email,
                ...passwordHashing
            })

            if (!user) {
                throw new Error("Couldn't create a new User")
            }

            return userResponse(res, user._id);
            
        } catch (err) {
            console.error(err)
            return response505(res);
        }
    }

    static async forgotPassword (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = forgotPasswordValidation(body);

            //Respond with an error msg if validation fails
            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { email } = body;

            //Email Exist Check
            const emailExistCheck = await emailExist(email);
            if (!emailExistCheck) {
                return errorResponse(res, 400, "Email not registered with us");
            }

            const user = await User.findOne({
                email : email
            }).exec();

            //Generate the Password Code
            const generatedCode = await user.generatePasswordCode();

            //Send Mail
            const emailSentDetail = await passwordeCodeMail(generatedCode.code, user.email)
            
            return successResponse(res, 'Your code has been sent to your mail');

        } catch (err) {
            console.error(err)
            return response505(res);
        }
    }

    static async passwordCodeConfirmation (req, res) {
        try {
            const { body } = req;

            //Validation need to be done here
            const errors = passwordCodeConfirmValidation(body);

            //Respond with an error msg if validation fails
            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { email, password, code } = body;

             //Email Exist Check
             const emailExistCheck = await emailExist(email);
             if (!emailExistCheck) {
                 return errorResponse(res, 400, "Email not registered with us");
             }

             const user = await User.findOne({
                email : email
            }).exec();

            //Checking whether the code matches
            const codeMatches = user.checkPasswordCodeMatch(code);

            if(!codeMatches) {
                return errorResponse(res, 401, "Code does not match");
            }

            //Checking whether the code is still valid
            const isCodeValid = user.isPasswordCodeExpired();

            if (!isCodeValid) {
                return errorResponse(res, 401, "This code has expired");
            }

            //Changing the Password
            const updatePasswordDetails = await user.createPassword(password);

            //Force expire code
            const expireCode = await user.expireCode();

            return successResponse(res, 'Password has been changed')
        } catch (err) {
            console.error(err)
            return response505(res);
        }
    }
}