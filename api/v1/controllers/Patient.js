import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';
import listUsersResponse from '../responses/listUsersResponse'

import User from '../../../models/User';
import Service from '../../../models/Service';

import updateLocationValidation from '../validations/updateLocation';


export default class Partner {
    static async listPatients (req, res) {
        try {
            const userId = req.app.get('userId');

            const { page=1, limit=10, latitude, longitude } = req.query;

            let options = {
                res,
                page, 
                limit,
                type: 'patient'
            }

            if (latitude && longitude) {
                const location = {
                    latitude,
                    longitude
                }

                const errors = updateLocationValidation(location);

                if (errors) {
                    return errorResponse(res, 400, errors);
                }

                options = { ...options, location }
            } else {
                const user = await User.findById(userId).exec();
                if (user) {
                    const check1 = user.location && user.location.coordinates && user.location.coordinates[0];
                    const check2 = user.location && user.location.coordinates && user.location.coordinates[1];
                    if (check1 && check2) {
                        const location = {
                            latitude: user.location.coordinates[1],
                            longitude: user.location.coordinates[0]
                        }
                        options = { ...options, location }
                    } 
                }
            }
            
            return listUsersResponse(options);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    
    
}