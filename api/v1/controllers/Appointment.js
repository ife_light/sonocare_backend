import moment from 'moment';

import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';
import appointmentResponse from '../responses/appointmentResponse';

import User from '../../../models/User';
import Appointment from '../../../models/Appointment';
import Service from '../../../models/Service';
import SubService from '../../../models/SubService';
import History from '../../../models/History';

import createAppointmentValidation from '../validations/createAppointment';
import rescheduleAppointmentValidation from '../validations/rescheduleAppointment';

import notificationHandler from './../../../services/notificationHandler';


export default class {
    static async createAnAppointment (req, res) {
        try {
            const userId = req.app.get('userId');

            const { partnerId, serviceId } = req.params;

            const user = await User.findById(userId).exec();
            if (user.type !== "patient") {
                return errorResponse(res, 403, "You are forbidden for creating an appointment");
            }

            const partnerExist = await User.findOne({
                type: 'partner',
                _id: partnerId
            }).exec();

            if (!partnerExist) {
                return errorResponse(res, 400, "Invalid partner ID")
            }

            if (!partnerExist.active) {
                return errorResponse(res, 400, "This partner is not yet activated");
            }
            const validService = await Service.findById(serviceId).exec();
            const validSubService = await SubService.findById(serviceId).exec();

            if (!validService && !validSubService) {
                return errorResponse(res, 400, "The service/sub-service with that ID does not exist");
            }

            let additionalServiceFields = {};

            if (validService) {
                additionalServiceFields = {
                    service: serviceId,
                    subService: null,
                }
            } else {
                additionalServiceFields = {
                    service: validSubService.service,
                    subService: serviceId,
                }
            }

            const { body } = req;

            const errors = createAppointmentValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { description, scheduled_date } = body;

            const patientScheduledDate = new Date(moment(scheduled_date, 'YYYY-MM-DD hh:mm:ss'))

            if (patientScheduledDate <= new Date(Date.now()) ) {
                return errorResponse(res, 400, "the scheduled date must be later than this date")
            }

            const appointmentStatus = 'patient-initiated';
            const historyType = "appointment";

            const newAppointment = await Appointment.create({
                patientId: userId,
                partnerId,
                description,
                patientScheduledDate,
                status: appointmentStatus,
                ...additionalServiceFields
            })

            const theAppointment = await Appointment.findById(newAppointment._id).exec();

            const newHistory = await History.create({
                partner: partnerId,
                patient: userId,
                type: historyType,
                status: appointmentStatus,
                appointment: newAppointment._id,
            })

            if (partnerExist.fcmNotificationId) {
                notificationHandler({
                    appointment: theAppointment,
                    partner: partnerExist,
                    patient: user,
                    type: historyType,
                    to: partnerId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: newAppointment._id,
                        category: historyType,
                        action: appointmentStatus
                    },
                    fcmNotificationId: partnerExist.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Appointment created", appointmentId: newAppointment._id});

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async partnerAppointmentsView (req, res) {
        try {
            const partnerId = req.app.get('userId');

            const { service, limit=100, page=1, status  } = req.query;

            if (service) {
                const validServiceId = await Service.findById(service).exec();

                if (!validServiceId) {
                    return errorResponse(res, 400, "The service with that ID does not exist")
                }
            }

            return appointmentResponse({res, partnerId, status, limit, page, service });

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async partnerAppointmentsViewSingle (req, res) {
        try {
            const partnerId = req.app.get('userId');

            const { appointmentId } = req.params;

            const appointmentExist = await Appointment.findById(appointmentId).exec();

            if (!appointmentExist) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            return appointmentResponse({ res, appointmentId });
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async adminListAppointments (req, res) {
        try {

            const { service, limit=100, page=1, status  } = req.query;

            let serviceType;

            if (service) {
                const validServiceId = await Service.findById(service).exec();
                const validSubServiceId = await SubService.findById(service).exec();

                if (!validServiceId && !validSubServiceId) {
                    return errorResponse(err, 400, "The service or subservice with that ID does not exist")
                }

                if (validServiceId) {
                    serviceType = 'service';
                } else {
                    serviceType = 'subService';
                }
            }

            return appointmentResponse({res, status, limit, page, service, serviceType });

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async acceptAppointment (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;

            const partner = await User.findById(userId).exec();

            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            if (appointment.status !== 'patient-initiated') {
                return errorResponse(res, 400, "Action already taken by partner")
            }

            if (appointment.partnerId != userId) {
                return errorResponse(res, 403, "You are forbidden to accept this appointment")
            }

            if (!partner.active) {
                return errorResponse(res, 400, "This partner has been deactivated")
            }

            const appointmentStatus = 'partner-accepted';
            const historyType = "appointment";

            const updateDetails = await Appointment.findByIdAndUpdate(appointmentId, {
                status: appointmentStatus,
                partnerAcceptedDate: new Date(),
                partnerAccepted: true,
                appointmentDate: appointment.patientScheduledDate,
            })

            
            const patient = await User.findById(appointment.patientId).exec();
            const theAppointment = await Appointment.findById(appointmentId).exec()

            const newHistory = await History.create({
                partner: appointment.partnerId,
                patient: appointment.patientId,
                type: historyType,
                status: appointmentStatus,
                appointment: appointment._id,
            })
            
            if (patient.fcmNotificationId) {
                notificationHandler({
                    patient,
                    partner,
                    appointment: theAppointment,
                    type: historyType,
                    to: appointment.patientId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: appointment._id,
                        category: historyType,
                        action: appointmentStatus
                    },
                    fcmNotificationId: patient.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Appointment accepted", appointmentId: appointment._id});
            
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async declineAnAppointment (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;
            const partner = await User.findById(userId).exec();

            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            if (appointment.status !== 'patient-initiated') {
                return errorResponse(res, 400, "Action already taken by partner")
            }

            if (appointment.partnerId != userId) {
                return errorResponse(res, 403, "You are forbidden to decline this appointment")
            }

            if (!partner.active) {
                return errorResponse(res, 400, "This partner has been deactivated")
            }

            const appointmentStatus = 'partner-declined';
            const historyType = "appointment";

            const updateDetails = await Appointment.findByIdAndUpdate(appointmentId, {
                status: appointmentStatus,
                partnerRejectedDate: new Date(),
                partnerRejected: true
            })

            const newHistory = await History.create({
                partner: appointment.partnerId,
                patient: appointment.patientId,
                type: historyType,
                status: appointmentStatus,
                appointment: appointment._id,
            })

            const patient = await User.findById(appointment.patientId).exec();
            const theAppointment = await Appointment.findById(appointmentId).exec()

            if (patient.fcmNotificationId) {
                notificationHandler({
                    partner,
                    patient,
                    appointment: theAppointment,
                    type: historyType,
                    to: appointment.patientId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: appointment._id,
                        category: 'appointment',
                        action: appointmentStatus
                    },
                    fcmNotificationId: patient.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Appointment rejected successfully", appointmentId: appointment._id});
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async rescheduleAppointment (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;

            const partner = await User.findById(userId).exec();
            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            if (appointment.status !== 'patient-initiated') {
                return errorResponse(res, 400, "Action already taken by partner")
            }

            if (appointment.partnerId != userId) {
                return errorResponse(res, 403, "You are forbidden to reschedule this appointment")
            }

            if (!partner.active) {
                return errorResponse(res, 400, "This partner has been deactivated")
            }

            const { body } = req;

            const errors = rescheduleAppointmentValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { reason, rescheduled_date} = body;

            const partnerRescheduledDate = new Date(moment(rescheduled_date, 'YYYY-MM-DD hh:mm:ss'))

            const appointmentStatus = 'partner-rescheduled';
            const historyType = "appointment";

            const updateDetails = await Appointment.findByIdAndUpdate(appointmentId, {
                status: appointmentStatus,
                partnerRescheduledDate: new Date(),
                partnerRescheduled: true,
                partnerRescheduledTo: partnerRescheduledDate,
                partnerRescheduledReason: reason
            })

            const newHistory = await History.create({
                partner: appointment.partnerId,
                patient: appointment.patientId,
                type: historyType,
                status: appointmentStatus,
                appointment: appointment._id,
            })

            const patient = await User.findById(appointment.patientId).exec();
            const theAppointment = await Appointment.findById(appointmentId).exec()

            if (patient.fcmNotificationId) {
                notificationHandler({
                    partner,
                    patient,
                    type: historyType,
                    appointment: theAppointment,
                    to: appointment.patientId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: appointment._id,
                        category: historyType,
                        action: appointmentStatus
                    },
                    fcmNotificationId: patient.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Appointment Rescheduled successfully", appointmentId: appointment._id});
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async patientAppointmentsView (req, res) {
        try {
            const patientId = req.app.get('userId');

            const { service, limit=100, page=1, status  } = req.query;

            if (service) {
                const validServiceId = await Service.findById(service).exec();

                if (!validServiceId) {
                    return errorResponse(res, 400, "The service with that ID does not exist")
                }
            }

            return appointmentResponse({res, patientId, status, limit, page, service });
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async patientAppointmentViewSingle (req, res) {
        try {
            const partnerId = req.app.get('userId');

            const { appointmentId } = req.params;

            const appointmentExist = await Appointment.findById(appointmentId).exec();

            if (!appointmentExist) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            return appointmentResponse({ res, appointmentId });
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async acceptReschedule (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;

            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            if (appointment.status !== 'partner-rescheduled') {
                return errorResponse(res, 400, "Action already taken by patient")
            }

            const appointmentStatus = 'patient-accepted-reschedule';
            const historyType = "appointment";

            const updateDetails = await Appointment.findByIdAndUpdate(appointmentId, {
                status: appointmentStatus,
                patientAcceptRescheduledDate: new Date(),
                patientAcceptRescheduled: true,
                appointmentDate: appointment.partnerRescheduledTo
            })

            const patient = await User.findById(userId).exec();
            const partner = await User.findById(appointment.partnerId).exec();
            const theAppointment = await Appointment.findById(appointmentId).exec()

            const newHistory = await History.create({
                partner: appointment.partnerId,
                patient: appointment.patientId,
                type: historyType,
                status: appointmentStatus,
                appointment: appointment._id,
            })

            if (partner.fcmNotificationId) {
                notificationHandler({
                    partner,
                    patient,
                    appointment: theAppointment,
                    type: historyType,
                    to: appointment.partnerId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: appointment._id,
                        category: historyType,
                        action: appointmentStatus
                    },
                    fcmNotificationId: partner.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Appointment Rescheduled accepted successfully", appointmentId: appointment._id});
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async rejectReschedule (req, res) {
        try {
            const userId = req.app.get('userId');
            const { appointmentId } = req.params;

            const appointment = await Appointment.findById(appointmentId).exec();

            if (!appointment) {
                return errorResponse(res, 404, "Appointment does not exist");
            }

            if (appointment.status !== 'partner-rescheduled') {
                return errorResponse(res, 400, "Action already taken by patient")
            }

            const appointmentStatus = 'patient-declined-reschedule';
            const historyType = "appointment";

            const updateDetails = await Appointment.findByIdAndUpdate(appointmentId, {
                status: appointmentStatus,
                patientDeclineRescheduledDate: new Date(),
                patientDeclineRescheduled: true
            })  

            const patient = await User.findById(userId).exec();
            const partner = await User.findById(appointment.partnerId).exec();
            const theAppointment = await Appointment.findById(appointmentId).exec()

            const newHistory = await History.create({
                partner: appointment.partnerId,
                patient: appointment.patientId,
                type: historyType,
                status: appointmentStatus,
                appointment: appointment._id,
            })

            if (partner.fcmNotificationId) {
                notificationHandler({
                    type: historyType,
                    patient,
                    partner,
                    appointment: theAppointment,
                    to: appointment.partnerId,
                    eventType: appointmentStatus,
                    data: {
                        appointment_id: appointment._id,
                        category: historyType,
                        action: appointmentStatus
                    },
                    fcmNotificationId: partner.fcmNotificationId
                })
            }

            return appointmentResponse({res, message: "Rescheduled appointment declined successfully", appointmentId: appointment._id});
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}