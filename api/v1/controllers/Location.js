import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';

import updateLocationValidation from '../validations/updateLocation';


export default class Location {
    static async updateUserLocation (req, res) {
        try {
            const userId = req.app.get('userId');

            const { body } = req;

            //Validation need to be done here
            const errors = updateLocationValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { latitude, longitude } = req.body;

            const updatedLocation = await User.findByIdAndUpdate(userId, {
                $set: {
                    'location.type': 'Point',
                    'location.coordinates.0': longitude,
                    'location.coordinates.1': latitude
                }
            })

            return successResponse(res, "Location updated successfully")
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}