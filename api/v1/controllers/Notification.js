import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';


export default class Notification {
    static async updateUserRegistrationId (req, res) {
        try {
            const userId = req.app.get('userId');

            const { body } = req;

            const { registration_id } = body;

            if (!registration_id) {
                return errorResponse(res, 400, "Registration ID is required")
            }

            const user = await User.findById(userId).exec();

            const updated_details = await user.updateFCMRegistrationId(registration_id);

            return successResponse(res, "User FCM registation ID updated successfully")
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}