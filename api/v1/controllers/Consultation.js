import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';
import History from '../../../models/History';

import notificationHandler from './../../../services/notificationHandler';


export default class Consultation {
    static async makeCall (req, res) {
        try {
            const userId = req.app.get('userId');

            const { params } = req;
            const { toCall } = params;

            const toCallUser = await User.findById(toCall).exec();

            if (!toCallUser) {
                return errorResponse(res, 400, "The user to be called is not valid")
            }

            if (toCallUser && toCallUser.type === 'partner' && !toCallUser.active) {
                return errorResponse(res, 400, "The partner is not yet activated");
            }

            const user = await User.findById(userId).exec();

            const consultationStatus = 'initiate-call';
            const historyType = "consultation";

            let callerName = (user.type === "patient") ? `${user.firstName} ${user.lastName}` : user.institutionName;
            callerName = (user.type === "admin") ? "Admin" : callerName;

            const notificationData = {
                type: historyType,
                event: consultationStatus,
                caller_id: userId,
                callee_id: toCall,
                group: `${userId}${toCall}`,
                caller_name: callerName
            }

            const newHistory = await History.create({
                partner: toCall,
                patient: userId,
                type: historyType,
                status: consultationStatus,
            })

            //console.log(notificationData);
            if (toCallUser.fcmNotificationId) {
                notificationHandler({
                    type: historyType,
                    to: toCall,
                    eventType: consultationStatus,
                    data: notificationData,
                    fcmNotificationId: toCallUser.fcmNotificationId
                })
            } else {
                console.log(toCallUser);
                return errorResponse(res, 400, "Couldnt send call notification to user");
            }

            return successResponse(res, "Call successfully initiated", notificationData);
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async rejectCall (req, res) {
        try {
            const userId = req.app.get('userId');

            const { params, body } = req;
            const { caller } = params;
            const { reason } = body;

            const callerUser = await User.findById(caller).exec();

            if (!callerUser) {
                return errorResponse(res, 400, "The ID of user who called is not valid")
            }

            //const callee = await User.findById(userId).exec();

            let callerName = (callerUser.type === "patient") ? `${callerUser.firstName} ${callerUser.lastName}` : callerUser.institutionName;
            callerName = (callerUser.type === "admin") ? "Admin" : callerName;

            const consultationStatus = 'reject-call';
            const historyType = "consultation";

            const newHistory = await History.create({
                partner: userId,
                patient: caller,
                type: historyType,
                status: consultationStatus,
            })

            const notificationData = {
                type: historyType,
                event: consultationStatus,
                reason,
                caller_id: caller,
                callee_id: userId,
                group: `${caller}${userId}`,
                caller_name: callerName
            }

            //console.log(notificationData);
            if (callerUser.fcmNotificationId) {
                notificationHandler({
                    type: historyType,
                    to: caller,
                    eventType: consultationStatus,
                    data: notificationData,
                    fcmNotificationId: callerUser.fcmNotificationId
                })
            } else {
                return errorResponse(res, 400, "Couldnt send call notification to user");
            }

            return successResponse(res, "Call successfully rejected");
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async acceptCall (req, res) {
        try {
            const userId = req.app.get('userId');

            const { params } = req;
            const { caller } = params;

            const callerUser = await User.findById(caller).exec();

            if (!callerUser) {
                return errorResponse(res, 400, "The ID of user who called is not valid")
            }

            //const callee = await User.findById(userId).exec();

            const consultationStatus = 'accept-call';
            const historyType = "consultation";

            let callerName = (callerUser.type === "patient") ? `${callerUser.firstName} ${callerUser.lastName}` : callerUser.institutionName;
            callerName = (callerUser.type === "admin") ? "Admin" : callerName;

            const notificationData = {
                type: historyType,
                event: consultationStatus,
                caller_id: caller,
                callee_id: userId,
                group: `${caller}${userId}`,
                caller_name: callerName
            }

            const newHistory = await History.create({
                partner: userId,
                patient: caller,
                type: historyType,
                status: consultationStatus,
            })

            //console.log(notificationData);
            if (callerUser.fcmNotificationId) {
                notificationHandler({
                    type: historyType,
                    to: caller,
                    eventType: consultationStatus,
                    data: notificationData,
                    fcmNotificationId: callerUser.fcmNotificationId
                })
            } else {
                return errorResponse(res, 400, "Couldnt send call notification to user");
            }

            return successResponse(res, "Call successfully accepted");
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}