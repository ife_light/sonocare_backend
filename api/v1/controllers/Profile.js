import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';
import userResponse from '../responses/userResponse';

import getProfilePic from '../../../services/getProfilePicture';
import saveCloudinaryProfilePic from '../../../services/saveCloudinaryProfilePic';

import updatePartnerValidation from '../validations/updatePartner';
import updatePatientValidation from '../validations/updatePatient';

import { Types } from 'mongoose';

const ObjectId = Types.ObjectId;

export default class {
    static async getProfile (req, res) {
        try {
            let userId;

            if (!req.params.id) {
                 userId = req.app.get('userId');
            } else {
                if (!ObjectId.isValid(req.params.id)) {
                    return errorResponse(res, 400, "Invalid user Id")
                }
                userId = req.params.id;
            }
            return userResponse(res, userId, false);
        } catch (err) {
            console.error(err);
            return response505(res);
        }
    }

    static async getProfilePicture (req,res) {
        try {
            const { id } = req.params;
            let userId;
            if (id === undefined || id === null || id === "") {
                userId = req.app.get('userId');
            } else {
                if (!ObjectId.isValid(req.params.id)) {
                    return errorResponse(res, 400, "Invalid user Id")
                }
                userId = req.params.id;
            }
            const profilePicLink = await getProfilePic(userId);
                return successResponse(res, "Image Link Recieved", {
                    image_link: profilePicLink
                })
        } catch (err) {
            console.error(err);
            return response505(res);
        }
    }

    static async uploadProfilePicture (req, res) {
        try {
            const userId = req.app.get('userId');

            if (!req.file) {
                return errorResponse(res, 400, "No or invalid image")
            }
            const imageUrl = req.file.secure_url;
            const imageId = req.file.public_id;

            const storeImage = await saveCloudinaryProfilePic(userId, imageUrl, imageId);

            return successResponse(res, "The image has been uploaded successfully", {
                image_link: imageUrl
            })
            
        } catch (err) {
            console.error(err);
            return response505(res);
        }
    }

    static async updatePartnerProfile (req, res) {
        try {
            const userId = req.app.get('userId');

            const user = await User.findById(userId).exec()

            if (user.type !== 'partner') {
                return errorResponse(res, 403, "Not a partner")
            }

            const {
                phone,
                institution_address,
                institution_name,
                institution_type
            } = req.body

            const errors = updatePartnerValidation(req.body);
            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const updateObject = {};
            if (phone) {
                updateObject['phone'] = phone
            }

            if (institution_address) {
                updateObject['institutionAddress'] = institution_address;
            }

            if (institution_name) {
                updateObject['institutionName'] = institution_name;
            }

            if (institution_type) {
                updateObject['institutionType'] = institution_type;
            }

            const updatePartner = await User.findByIdAndUpdate(userId, updateObject ).exec();

            return successResponse(res, "Partner profile updated successfully");

        } catch (err) {
            console.error(err);
            return response505(res);
        }
    }

    static async updatePatientProfile (req, res) {
        try {
            const userId = req.app.get('userId');

            const user = await User.findById(userId).exec()

            if (user.type !== 'patient') {
                return errorResponse(res, 403, "Not a patient")
            }

            const {
                first_name,
                last_name,
                phone,
                address,
                date_of_birth,
                sex,
                income_range_max,
                income_range_min,
                income_range_currency
            } = req.body

            const errors = updatePatientValidation(req.body);
            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const updateObject = {};
            if (first_name) {
                updateObject['firstName'] = first_name
            }

            if (last_name) {
                updateObject['lastName'] = last_name;
            }

            if (phone) {
                updateObject['phone'] = phone;
            }

            if (address) {
                updateObject['address'] = address;
            }
            if (date_of_birth) {
                updateObject['dateOfBirth'] = date_of_birth;
            }
            if (sex) {
                updateObject['sex'] = sex;
            }

            if (income_range_max || income_range_min || income_range_currency) {
                updateObject['incomeRange'] = {};
            }
            if (income_range_max) {
                updateObject['incomeRange']['max'] = income_range_max;
            }
            if (income_range_min) {
                updateObject['incomeRange']['min'] = income_range_min;
            }
            if (income_range_currency) {
                updateObject['incomeRange']['currency'] = income_range_currency;
            }

            const updatePatient = await User.findByIdAndUpdate(userId, updateObject ).exec();

            return successResponse(res, "Patient profile updated successfully");

        } catch (err) {
            console.error(err);
            return response505(res);
        }
    }
}