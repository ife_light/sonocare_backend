import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';


import User from '../../../models/User';
import Appointment from '../../../models/Appointment';
import Report from '../../../models/Report';
import File from '../../../models/File';


import notificationHandler from './../../../services/notificationHandler';


export default class {
   
    static async graphHome (req, res) {
        try {
            
            const userStat = await User.aggregate([
                {$group: {
                    _id: {$month: '$createdAt'},
                    total: {$sum: 1},
                }},
                {$sort: {_id: 1}}
            ])

            const data = {};

            const rawData = [
                ['January', 0],
                ['February', 0],
                ['March', 0],
                ['April', 0],
                ['May', 0],
                ['June', 0],
                ['July', 0],
                ['August', 0],
                ['September', 0],
                ['October', 0],
                ['November', 0],
                ['December', 0],
            ];

            for (const dt of userStat) {
                rawData[dt['_id'] - 1][1] = dt['total']
            }

            for (let item of rawData) {
              data[item[0]] = item[1];
            }
            
            return successResponse(res, "Mothly statistics", data);


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}