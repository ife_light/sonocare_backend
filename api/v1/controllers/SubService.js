import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';

import { emailExist } from '../../../services/emailExist';

import Service from '../../../models/Service';
import SubService from '../../../models/SubService';

import createSubServiceValidation from '../validations/createSubService';
import updateSubServiceValidation from '../validations/updateSubService';

import mongoose from 'mongoose';

const ObjectId = mongoose.Types.ObjectId;


export default class {
    static async listSubServices (req, res) {
        try {
            const { serviceId } = req.params;

            const validServiceId = await Service.findById(serviceId).exec();
            const validSubServiceId = await SubService.findById(serviceId).exec();

            if (!validServiceId && !validSubServiceId) {
                return errorResponse(res, 400, "The service/sub-service with that ID does not exist")
            }

            const { page=1, limit=20, home_service } = req.query;

            let homeServiceQuery = {};
            let additionalServiceQuery = {};
            if (home_service) {
                const homeService = home_service !== undefined && (home_service === "true" || home_service === true) ? true : false;
                homeServiceQuery = { homeService }
            }

            if (validServiceId) {
                additionalServiceQuery = {
                    service: ObjectId(serviceId),
                    parent: null,
                }
            } else {
                additionalServiceQuery = {
                    parent: ObjectId(serviceId),
                }
            }

            const count = await SubService.countDocuments({
                ...additionalServiceQuery,
                ...homeServiceQuery,
            }).exec();
            
            let pages = Math.ceil(count / limit);
            const toSkip = limit * (page - 1);

            const subServices = await SubService.find({
                ...additionalServiceQuery,
                ...homeServiceQuery,
            })
            .populate("children")
            .limit(limit)
            .skip(toSkip)
            .exec()

            return successResponse(res, "Subservices retrieved successfully", subServices, {
                page,
                pages
            })

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async createSubService (req, res) {
        try {
            const { body } = req;

            const { serviceId } = req.params;

            //Validation need to be done here
            const errors = createSubServiceValidation(body);

            const validServiceId = await Service.findById(serviceId).exec();
            const validSubServiceId = await SubService.findById(serviceId).exec();

            if (!validServiceId && !validSubServiceId) {
                return errorResponse(res, 400, "The service/sub-service with that ID does not exist")
            }

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            //Creating chances for sub-services to have parents
            let parentAdditionalField = {};
            if (validSubServiceId) {
                parentAdditionalField = {
                    parent: validSubServiceId,
                }
            }

            const serviceFieldValue = validServiceId ? validServiceId._id : validSubServiceId.service ;

            const { name, description, picture, amount, country, home_service } = req.body;

            const homeService = home_service !== undefined && (home_service === "true" || home_service === true) ? true : false;

            const nameExist = await SubService.findOne({
                service: serviceId,
                name
            }).exec();

            if (nameExist) {
                return errorResponse(res, 400, "Subservice with that name already exist");
            }

            const newSubService = await SubService.create({
                service: serviceFieldValue,
                homeService,
                name,
                description,
                picture,
                amount,
                country,
                ...parentAdditionalField
            })

            if (validSubServiceId) {
                const updateDetails = await SubService.findOneAndUpdate({
                    _id: validSubServiceId._id
                }, {
                    $push: { children: newSubService._id}
                }).exec()
            }

            return successResponse(res, "New subservice created", newSubService)


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async uploadSubServiceImage (req, res) {
        try {
            const { subServiceId } = req.params;

            const validSubServiceId = await SubService.findById(subServiceId).exec();

            if (!validSubServiceId) {
                return errorResponse(res, 400, "The subservice with that ID does not exist")
            }

            if (!req.file) {
                return errorResponse(res, 400, "No or invalid image")
            }
            const imageUrl = req.file.secure_url;
            const imageId = req.file.public_id;

            const updateDetails = await SubService.findByIdAndUpdate(subServiceId, {
                picture: imageUrl
            })

            const subService = await SubService.findById(subServiceId).exec();
            return successResponse(res, "Image updated successfully", subService)
        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async updateSubService (req, res) {
        try {
            const { body } = req;

            const { subServiceId } = req.params;

            const validSubService = await SubService.findById(subServiceId).exec();

            if (!validSubService) {
                return errorResponse(res, 400, "The subservice with that ID does not exist")
            }

            //Validation need to be done here
            const errors = updateSubServiceValidation(body);

            if (errors) {
                return errorResponse(res, 400, errors);
            }

            const { name, home_service } = req.body;

            if (name && name != validSubService.name) {
                const nameExist = await SubService.findOne({
                    name
                }).exec();
    
                if (nameExist) {
                    return errorResponse(res, 400, "Subservice with that name already exist");
                }
            }

            let homeServiceField = {}
            if (home_service) {
                const homeService = home_service !== undefined && (home_service === "true" || home_service === true) ? true : false;
                homeServiceField = { homeService }
            }

            const updatedServiceDetails = await SubService.findByIdAndUpdate(subServiceId, {
                ...req.body,
                ...homeServiceField
            })

            const subService = await SubService.findById(subServiceId).exec();

            return successResponse(res, "Subservice updated successfully", subService);

        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }

    static async deleteSubService (req, res) {
        try {
            const { subServiceId } = req.params;

            const validSubServiceId = await SubService.findById(subServiceId).exec();

            if (!validSubServiceId) {
                return errorResponse(res, 400, "The subservice with that ID does not exist")
            }

            if (validSubServiceId.parent) {
                const updateDetails = await SubService.findOneAndUpdate({
                    _id: validSubServiceId.parent
                }, {
                    $pull: {children: subServiceId}
                })
            }

            const deletedSubServiceDetails = await SubService.findByIdAndDelete(subServiceId).exec();

            return successResponse(res, "Sub Service deleted successfully");


        } catch (err) {
            console.log(err);
            return response505(res);
        }
    }
    
}