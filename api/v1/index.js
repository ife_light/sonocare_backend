import { Router } from 'express';
import cors from 'cors';

import Authentication from './controllers/Authentication';
import Profile from './controllers/Profile';
import Admin from './controllers/Admin';
import Appointment from './controllers/Appointment';
import Partner from './controllers/Partner';
import Patient from './controllers/Patient';
import Location from './controllers/Location';
import Service from './controllers/Service';
import Notification from './controllers/Notification';
import SubService from './controllers/SubService'
import Consultation from './controllers/Consultation';
import Report from './controllers/Report';
import History from './controllers/History';
import Utils from './controllers/Utils';

import isAuthenticated from './policies/isAuthenticated';
import isAdmin from './policies/isAdmin';
import validFileId from './policies/validFileId';
import validReportId from './policies/validReportId';
import isSuperAdmin from './policies/isSuperAdmin';

import profilePicUploader from './middlewares/profilePicUpload';
import serviceImageUploader from './middlewares/serviceImageUpload';
import subServiceImageUploader from './middlewares/subServiceImageUpload';
import reportFileUploader from './middlewares/reportFileUpload';

//v1 Api Router
const v1app = Router();

// Use cors for cross origin request for api
v1app.use(cors());

v1app.get('/', (req, res) => {
    res.send("V1 app working")
})

//Authentication
v1app.post('/patient/signup', Authentication.signupPatient); 
v1app.post('/partner/signup', Authentication.signupPartner);
v1app.post('/login', Authentication.login);
v1app.post('/forgot-password', Authentication.forgotPassword);
v1app.post('/confirm-password', Authentication.passwordCodeConfirmation)

//Admin 
v1app.route('/admin')
    .post(isAuthenticated, isSuperAdmin, Admin.createAdmin)
    .get(isAuthenticated, isAdmin, Admin.listAdmins)
v1app.get('/admin/initiate-first-admin', Admin.initiateFirstAdmin)
v1app.post('/admin/make/:userId', isAuthenticated, isSuperAdmin, Admin.makeAdmin)
v1app.post('/admin/unmake/:userId', isAuthenticated, isSuperAdmin, Admin.unmakeAdmin)
v1app.delete('/admin/:userId', isAuthenticated, isSuperAdmin, Admin.deleteAdmin)
v1app.post('/admin/partner/activate/:partnerId', isAuthenticated, isAdmin, Admin.activatePartner)
v1app.post('/admin/partner/deactivate/:partnerId', isAuthenticated, isAdmin, Admin.deactivatePartner)

//Location
v1app.post('/profile/location', isAuthenticated, Location.updateUserLocation);

//Profile Picture
v1app.get('/profile', isAuthenticated, Profile.getProfile);
v1app.get('/profile/picture', isAuthenticated,  Profile.getProfilePicture);
v1app.get('/profile/picture/:id',  Profile.getProfilePicture);
v1app.post('/profile/picture', isAuthenticated, profilePicUploader.single('image'), Profile.uploadProfilePicture);
v1app.get('/profile/:id', Profile.getProfile);
v1app.post('/profile/partner', isAuthenticated, Profile.updatePartnerProfile);
v1app.post('/profile/patient', isAuthenticated, Profile.updatePatientProfile);

//Services
v1app.route('/service')
    .get(Service.listServices)
    .post(isAuthenticated, isAdmin, Service.createService);
v1app.post('/service/image/:serviceId', isAuthenticated, isAdmin, serviceImageUploader.single('image'), Service.uploadServiceImage);
v1app.route('/service/:serviceId')
    .put(isAuthenticated, isAdmin, Service.updateService)
    .delete(isAuthenticated, isAdmin, Service.deleteService)

//Sub Services
v1app.route('/sub-service/:serviceId')
    .get(SubService.listSubServices)
    .post(isAuthenticated, isAdmin, SubService.createSubService);
v1app.post('/sub-service/image/:subServiceId', isAuthenticated, isAdmin, subServiceImageUploader.single('image'), SubService.uploadSubServiceImage);
v1app.route('/sub-service/:subServiceId')
    .put(isAuthenticated, isAdmin, SubService.updateSubService)
    .delete(isAuthenticated, isAdmin, SubService.deleteSubService)

//Partners
v1app.post('/partner/service/add/:serviceId', isAuthenticated, Partner.addService)
v1app.post('/partner/service/remove/:serviceId', isAuthenticated, Partner.removeService)
v1app.get('/partner/service/', isAuthenticated, Partner.listServices)
v1app.get('/partner/subservice/', isAuthenticated, Partner.listSubServices)
v1app.get('/partner', Partner.listPartners)

//Patients
v1app.get('/patient', Patient.listPatients);
    
//Appointments
v1app.get('/appointment/partner', isAuthenticated, Appointment.partnerAppointmentsView)
v1app.get('/appointment/partner/:appointmentId', isAuthenticated, Appointment.partnerAppointmentsViewSingle)
v1app.post('/appointment/partner/accept/:appointmentId', isAuthenticated, Appointment.acceptAppointment)
v1app.post('/appointment/partner/decline/:appointmentId', isAuthenticated, Appointment.declineAnAppointment)
v1app.post('/appointment/partner/reschedule/:appointmentId', isAuthenticated, Appointment.rescheduleAppointment)
v1app.post('/appointment/patient/reschedule/accept/:appointmentId', isAuthenticated, Appointment.acceptReschedule)
v1app.post('/appointment/patient/reschedule/decline/:appointmentId', isAuthenticated, Appointment.rejectReschedule)
v1app.get('/appointment/patient', isAuthenticated, Appointment.patientAppointmentsView)
v1app.get('/appointment/patient/:appointmentId', isAuthenticated, Appointment.patientAppointmentViewSingle)
v1app.post('/appointment/:partnerId/:serviceId', isAuthenticated, Appointment.createAnAppointment)
v1app.get('/appointment/admin', isAuthenticated, isAdmin, Appointment.adminListAppointments)

//Notification
v1app.post('/notification/app/registration', isAuthenticated, Notification.updateUserRegistrationId);

//Constultation Calls
//Strictly for mobile apps Now
v1app.post('/consultation/call/initiate/:toCall', isAuthenticated, Consultation.makeCall);
v1app.post('/consultation/call/reject/:caller', isAuthenticated, Consultation.rejectCall);
v1app.post('/consultation/call/accept/:caller', isAuthenticated, Consultation.acceptCall);

//Reports
v1app.route('/report')
    .get( isAuthenticated, Report.listReports)
v1app.route('/report/appointment/:appointmentId')
    .post(isAuthenticated, Report.createReport)
    .get(isAuthenticated, Report.viewAReportByAppointment)
v1app.route('/report/:reportId')
    .post(validReportId, isAuthenticated, Report.updateReport)
    .get(validReportId, isAuthenticated, Report.viewAReport)
    .delete(validReportId, isAuthenticated, Report.deleteReport)
v1app.post('/report/:reportId/file', validReportId, isAuthenticated, reportFileUploader.single('file'), Report.addReportFile)
v1app.delete('/report/:reportId/file/:fileId', validReportId, validFileId, isAuthenticated, Report.removeReportFile)

//Histories
v1app.route('/history')
    .get( isAuthenticated, History.getHistories)

v1app.get('/stats/graph/monthly', Utils.graphHome)

export default v1app;