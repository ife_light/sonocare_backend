import mailer from '../../../providers/mailer'

/**
 * For sending password recovery mails
 * @param code {String} - The code to be sent
 * @param to {String} - The email to be sent to
 * @returns {Promise} - It returns a promise
 */
export default (code, to) => {
    const statement = `
    Use the code below to recover your password, The password is valid for a single day

    Code: ${code}
    `;
    const subject = 'Sonocare Password Recovery';
    return mailer(subject, to, statement, statement);
}