import User from '../../../models/User';
import { signID } from '../../../providers/token';

import mongoose from 'mongoose';

const ObjectId = mongoose.Types.ObjectId;

/**
 * Returns a User data
 * @param {Object} res - Express response object
 * @param {String} id - the user ID
 * @param {Boolean} embedToken - embed token in the user response
 * @returns {Promise} - The response
 */
const userResponse =  async (res, id, embedToken=true) => {
    const theUser = await User.aggregate([
        {$match : {_id : ObjectId(id)}},
        {$lookup: {
            from: 'services',
            localField: 'services',
            foreignField: '_id',
            as: 'services'
        }},
        {$lookup: {
            from: 'subservices',
            localField: 'subServices',
            foreignField: '_id',
            as: 'sub_services'
        }},
        {$project: {
            first_name : "$firstName",
            last_name : "$lastName",
            address: 1,
            phone : 1,
            email : 1,
            date_of_birth : "$dateOfBirth",
            sex : "$sex",
            income_range : "$incomeRange",
            type : 1,
            isAdmin: 1,
            location: 1,
            active: 1,
            reg_on: "$createdAt",
            institution_name : "$institutionName",
            institution_type : "$institutionType",
            institution_address : "$institutionAddress",
            profile_picture: "$profilePicUrl",
            services: 1,
            subservices: "$sub_services",
            fcm_reg_id: "$fcmNotificationId"
        }}
    ]);


    if (!theUser) {
        throw new Error("Couldn't fetch new User")
    }
    
    let responseData = {
        message: 'Users retrieved successfully',
        data : theUser[0],
    }

    if (embedToken) {
        const token = signID(id);
        responseData = { ...responseData, token };
    }

    return res.status(200).json(responseData)
}

export default userResponse;