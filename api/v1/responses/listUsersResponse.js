import mongoose from 'mongoose';

import User from '../../../models/User';

const ObjectId = mongoose.Types.ObjectId


/**
 * User list Object
 * @param {Object} options  - Options for the query 
 */
const userResponse =  async ({res, user, active, message, type="partner", limit=20, page=1, location, service, serviceType = 'service'}) => {
    const query = { type };

    limit = Number(limit);
    page = Number(page);

    if (type === 'partner') {
        query.active = true ;
    }

    if (type === 'partner' && user && user === 'admin') {
        delete query.active;
    }

    if (type === 'admin') {
        query.isAdmin = true;
    }

    if (active && active == "true" ) {
        query.active = true
    }

    if (active && active == "false" ) {
        query.$or = [
                {active: {$exists: false}},
                {active: {$eq: null}},
                {active: {$eq: false}}
            ]
    }

    if (service) { //service is the ID of the service
        query[serviceType] = ObjectId(service);
    }

    let locInsert = [];
    if (location) {
        //loc = { location: {$near: [location.longitude, location.latitude]}}

       const loc = {$geoNear: {
            includeLocs: "location",
            distanceField: "dist.calculated",
            near: {type: 'Point', coordinates: [parseFloat(location.longitude), parseFloat(location.latitude)]},
            spherical: true,
            query
          }}
        locInsert = [loc];
    }

    const count = await User.countDocuments(query).exec();

    let pages = Math.ceil(count / limit);
    const toSkip = limit * (page - 1); 

    const theUsers = await User.aggregate([
        ...locInsert,//loc, //For Location matching
        {$match : query},
        {$lookup: {
            from: 'services',
            localField: 'services',
            foreignField: '_id',
            as: 'services'
        }},
        /*{$lookup: {
            from: 'subservices',
            localField: 'subServices',
            foreignField: '_id',
            as: 'sub_services'
        }}, */
        {$project: {
            first_name : "$firstName",
            last_name : "$lastName",
            address: 1,
            phone : 1,
            email : 1,
            date_of_birth : "$dateOfBirth",
            sex : "$sex",
            income_range : "$incomeRange",
            type : 1,
            isAdmin: 1,
            location: 1,
            active: 1,
            institution_name : "$institutionName",
            institution_type : "$institutionType",
            institution_address : "$institutionAddress",
            profile_picture: "$profilePicUrl",
            //services: 1,
            subservices: { $ifNull: [ "$sub_services", null ] },
        }},
        { $limit : toSkip + limit},
        { $skip : toSkip}
    ]);


    if (!theUsers) {
        throw new Error("Couldn't fetch any user")
    }
    
    let responseData = {
        message: message || 'Users retrieved successfully',
        data : theUsers,
        page,
        pages 
    } 

    return res.status(200).json(responseData)
}

export default userResponse;