import mongoose from 'mongoose';

import Appointment from '../../../models/Appointment';


const ObjectId = mongoose.Types.ObjectId;


/**
 * Appointment list Object
 * @param {Object} options  - Options for the query 
 */
const AppointmentResponse =  async ({res, message, appointmentId, status, limit=20, page=1, service, serviceType = 'service', partnerId, patientId}) => {
    let query = { };

    limit = Number(limit);
    page = Number(page);

    if (service) { //service is the ID of the service
        query = {...query, [serviceType] : { $elemMatch : ObjectId(service)} }
    }

    if (partnerId) {
        query = {...query, partnerId: ObjectId(partnerId) } 
    }

    if (patientId) {
        query = {...query, patientId: ObjectId(patientId) } 
    }

    if (status) {
        query = {...query, status } 
    }

    const count = await Appointment.count(query).exec();


    let pages = Math.ceil(count / limit);
    const toSkip = limit * (page - 1); 

    if (appointmentId) {
        query = {...query, _id: ObjectId(appointmentId)  } 
    }

    const theAppointments = await Appointment.aggregate([
        {$match : query},
        { $sort : { createdAt : -1} },
        {$lookup: {
            from: 'users',
            localField: 'partnerId',
            foreignField: '_id',
            as: 'partner'
        }},
        {$unwind: '$partner'},
        {$lookup: {
            from: 'users',
            localField: 'patientId',
            foreignField: '_id',
            as: 'patient'
        }},
        {$unwind: '$patient'},
        {$lookup: {
            from: 'services',
            localField: 'service',
            foreignField: '_id',
            as: 'service'
        }},
        {$unwind: '$service'},
        {$lookup: {
            from: 'subservices',
            localField: 'subService',
            foreignField: '_id',
            as: 'sub_service'
        }},
        {$unwind: {
            "path": "$sub_service",
            "preserveNullAndEmptyArrays": true
            }
        },
        {$project: {
            patient_scheduled_date : "$patientScheduledDate",
            partner_accepted_date : "$partnerAcceptedDate",
            partner_accepted : '$partnerAccepted',
            partner_rejected_date : '$partnerRejectedDate',
            partner_rejected : '$partnerRejected',
            partner_rescheduled_date : "$partnerRescheduledDate",
            partner_rescheduled : "$partnerRescheduled",
            partner_rescheduled_to : '$partnerRescheduledTo',
            patient_accept_rescheduled_date : '$patientAcceptRescheduledDate',
            patient_accept_rescheduled: '$patientAcceptRescheduled',
            partner_rescheduled_reason: '$partnerRescheduledReason',
            patient_decline_rescheduled_date: "$patientDeclineRescheduledDate",
            patient_decline_rescheduled: "$patientDeclineRescheduled",
            appointment_date: '$appointmentDate',
            status: 1,
            subservice: { $ifNull: [ "$sub_service", null ] },
            'partner.first_name' : "$partner.firstName",
            'partner.last_name' : "$partner.lastName",
            'partner.address': 1,
            'partner.phone' : 1,
            'partner.email' : 1,
            'partner.date_of_birth' : "$partner.dateOfBirth",
            'partner.sex' : "$partner.sex",
            'patient.income_range' : "$patient.incomeRange",
            'partner.type' : 1,
            'partner.isAdmin': 1,
            'partner.location': 1,
            'partner.institution_name' : "$partner.institutionName",
            'partner.institution_type' : "$partner.institutionType",
            'partner.institution_address' : "$partner.institutionAddress",
            'partner.profile_picture': "$partner.profilePicUrl",
            'patient.first_name' : "$patient.firstName",
            'patient.last_name' : "$patient.lastName",
            'patient.address': 1,
            'patient.phone' : 1,
            'patient.email' : 1,
            'patient.date_of_birth' : "$patient.dateOfBirth",
            'patient.sex' : "$sex",
            'patient.income_range' : "$patient.incomeRange",
            'patient.type' : 1,
            'patient.isAdmin': 1,
            'patient.location': 1,
            'patient.institution_name' : "$patient.institutionName",
            'patient.institution_type' : "$patient.institutionType",
            'patient.institution_address' : "$patient.institutionAddress",
            'patient.profile_picture': "$patient.profilePicUrl",
        }},
        {$project: {
            "partner.services" : 0,
            "service.createdAt" : 0,
            "service.updatedAt" : 0,
            "subservice.createdAt" : 0,
            "subservice.updatedAt" : 0,
            "subservice.children" : 0,
        }},
        { $limit : toSkip + limit },
        { $skip : toSkip },
    ]);


    if (!theAppointments) {
        throw new Error("Couldn't fetch any Appointment")
    }

    if (appointmentId) {
        let responseData = {
            message: message || 'Appointments retrieved successfully',
            data : theAppointments[0],
        } 
    
        return res.status(200).json(responseData);
    }
    
    
    let responseData = {
        message: message || 'Appointments retrieved successfully',
        data : theAppointments, 
        page,
        pages 
    } 

    return res.status(200).json(responseData);
}

export default AppointmentResponse;