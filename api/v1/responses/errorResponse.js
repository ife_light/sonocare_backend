import { isArray } from "util";

/**
 * For sending Error responses with custom status and error
 * @param res {Object} - Express response Object
 * @param statusCode {Number} - Status code
 * @param message {String | Array} - The error message
 */
const errorResponse =  (res, statusCode, message) => {
    let errors = [];
    let msg;
    if (isArray(message)) {
        errors = [...message]
        msg = message[0]
    } else {
        errors = [message]
        msg = message
    }

    console.error(message);
    return res.status(statusCode).send({
        message : msg,
        errors
    })
}

export default errorResponse;