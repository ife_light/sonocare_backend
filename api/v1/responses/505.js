/**
 * 505 response message
 * @param res - express response object
 * @returns Object - returns a Json error message with505 status
 */
const error505 = function error505response (res)  {
    return res.status(500).json({
        message : "An error occurred on the server",
        data : null
    })
}


export default error505;