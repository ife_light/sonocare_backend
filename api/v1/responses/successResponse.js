
/**
 * This function is to send Success responses
 * @param {String} res - Response Oject from express server
 * @param {String} message - Message to be sent to the User
 * @param {String} data - Data to be sent to the user
 * @param {String} extraMeta - add extra meta data to the response
 */
const successResponse = (res, message, data = {}, extraMeta = {}) => {
    return  res.status(200).send({
        message,
        data,
        ...extraMeta
    })
}

export default successResponse;