import validate from 'validate.js';
import datetimeValidator from './validators/datetime';

validate.extend(validate.validators.datetime, datetimeValidator);

const constraints = {
    email: {
        presence: {
            message: "is required"
        },
        email: {
            message: "Not a valid email"
        }
    },
    password: {
        presence: {
            message: "is required"
        },
        length: {
            minimum: 6,
            message: "must be at least 6 characters"
        }
    },
    phone : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
    institution_address : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
    institution_name : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
    institution_type : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
};

/**
 * Login Vaidation function
 * @param {object} fields - the req.body from express
 * @returns {Array |null} - Null or otherwise the Array of errors
 */
export default function (fields) {
    const errors = validate(fields, constraints, {
        format: "flat"
    });
    return errors;

}