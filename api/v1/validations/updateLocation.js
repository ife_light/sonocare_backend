import validate from 'validate.js';

const constraints = {
    latitude: {
      presence: {
        message : " is required"
      },
      numericality: {
        greaterThan: -91,
        lessThanOrEqualTo: 90
      }
    },
    longitude: {
        presence: {
            message : " is required"
          },
          numericality: {
            greaterThan: -181,
            lessThanOrEqualTo: 180
          }
      },
    
  }

  /**
   * Update Location Validation function
   * @param {object} fields - the req.body from express
   * @returns {Array | null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
    const errors = validate(fields, constraints, {format: "flat"});
    return errors;

}