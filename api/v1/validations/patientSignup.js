import validate from 'validate.js';
import datetimeValidator from './validators/datetime';

validate.extend(validate.validators.datetime, datetimeValidator);
  

const constraints = {
    email: {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        email: {
            message: "Not a valid email"
        }
    },
    password: {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        length: {
            minimum: 6,
            message: "must be at least 6 characters"
        }
    },
    first_name : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    last_name : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    phone : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
    address : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
    },
    date_of_birth : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        datetime : true
    },
    sex : {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        inclusion : {
            within : ['male', 'female'],
            message : "should be either male or female"
        }
    },
    income_range_max : {
        numericality : {
            message : "should be a number"
        }
    },
    income_range_min : {
        numericality : {
            message : "should be a number"
        }
    },
    income_range_currency : {
        numericality : {
            message : "should be a number"
        }
    }
};

/**
 * Login Vaidation function
 * @param {object} fields - the req.body from express
 * @returns {Array |null} - Null or otherwise the Array of errors
 */
export default function (fields) {
    const errors = validate(fields, constraints, {
        format: "flat"
    });
    return errors;

}