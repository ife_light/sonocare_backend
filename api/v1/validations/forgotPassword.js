import validate from 'validate.js';

const constraints = {
    email: {
      presence: {
        message : "Email is required"
      },
      email : {
          message : "Not a valid email"
      }
    }
  }

  /**
   * Forgot Password Vaidation function
   * @param {object} fields - the req.body from express
   * @returns {Array | null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
    const errors = validate(fields, constraints, {format: "flat"});
    return errors;

}