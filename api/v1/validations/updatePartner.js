import validate from 'validate.js';
import datetimeValidator from './validators/datetime';

validate.extend(validate.validators.datetime, datetimeValidator);

const constraints = {
    phone : {
        length: {
            minimum: 3,
            message: "must be at least 3 characters"
          }
    },
    institution_address : {
        length: {
            minimum: 3,
            message: "must be at least 3 characters"
          }
    },
    institution_name : {
        length: {
            minimum: 3,
            message: "must be at least 3 characters"
          }
    },
    institution_type : {
        length: {
            minimum: 1,
            message: "must be at least 1 characters"
          }
    },
};

/**
 * Update Partner Vaidation function
 * @param {object} fields - the req.body from express
 * @returns {Array |null} - Null or otherwise the Array of errors
 */
export default function (fields) {
    const errors = validate(fields, constraints, {
        format: "flat"
    });
    return errors;

}