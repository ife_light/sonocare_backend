import validate from 'validate.js';
import datetimeValidator from './validators/datetime';

validate.extend(validate.validators.datetime, datetimeValidator);
  

const constraints = {
    first_name : {
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    last_name : {
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    phone : {
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    address : {
        length: {
            minimum: 1,
            message: "must be at least 1 character"
        }
    },
    date_of_birth : {
        datetime : true
    },
    sex : {
        inclusion : {
            within : ['male', 'female'],
            message : "should be either male or female"
        }
    },
    income_range_max : {
        numericality : {
            message : "should be a number"
        }
    },
    income_range_min : {
        numericality : {
            message : "should be a number"
        }
    },
    income_range_currency : {
        numericality : {
            message : "should be a number"
        }
    }
};

/**
 * Login Vaidation function
 * @param {object} fields - the req.body from express
 * @returns {Array |null} - Null or otherwise the Array of errors
 */
export default function (fields) {
    const errors = validate(fields, constraints, {
        format: "flat"
    });
    return errors;

}