import validate from 'validate.js';

const constraints = {
    reason: {
      length: {
        minimum: 1,
        message: "must be at least 1 characters"
      }
    },
    rescheduled_date: {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        datetime : true
    },
  }

  /**
   * Reschedule Appointment Vaidation function
   * @param {object} fields - the req.body from express
   * @returns {Array | null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
    const errors = validate(fields, constraints, {format: "flat"});
    return errors;

}