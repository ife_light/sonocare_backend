import validate from 'validate.js';

const constraints = {
    name: {
      length: {
        minimum: 3,
        message: "must be at least 3 characters"
      }
    },
    description: {
        length: {
          minimum: 3,
          message: "must be at least 3 characters"
        }
      },
    picture: {
        url: true
    },
    country: {
        length: {
            minimum: 3,
            message: "must be at least 3 characters"
          }
    },
    amount: {
        numericality: true
    },
  }

  /**
   * Create Service Vaidation function
   * @param {object} fields - the req.body from express
   * @returns {Array | null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
    const errors = validate(fields, constraints, {format: "flat"});
    return errors;

}