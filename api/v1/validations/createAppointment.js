import validate from 'validate.js';

const constraints = {
    description: {
      presence: {
        allowEmpty: false,
        message : " is required"
      },
      length: {
        minimum: 3,
        message: "must be at least 3 characters"
      }
    },
    scheduled_date: {
        presence: {
            allowEmpty: false,
            message: "is required"
        },
        datetime : true
    },
  }

  /**
   * Create Appointment Vaidation function
   * @param {object} fields - the req.body from express
   * @returns {Array | null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
    const errors = validate(fields, constraints, {format: "flat"});
    return errors;

}