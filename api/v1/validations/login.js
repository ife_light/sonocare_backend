import validate from 'validate.js';

const constraints = {
    email: {
      presence: {
        message : "Email is required"
      },
      email : {
          message : "Not a valid email"
      }
    },
    password: {
      presence: {
          message : "Password is required"
      },
      length: {
        minimum: 6,
        message: "must be at least 6 characters"
      }
    }
  };
  
  /**
   * Login Vaidation function
   * @param {object} fields - the req.body from express
   * @returns {Array |null} - Null or otherwise the Array of errors
   */
  export default function (fields) {
      const errors = validate(fields, constraints, {format: "flat"});
      return errors;

  }