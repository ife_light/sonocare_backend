import jwt from 'jsonwebtoken';
import config from '../../../config/default';

/* eslint-disable-next-line */
const isAuthenticated = function authenticationCheck(req, res) {
    try {
        const token = req.headers['x-access-token'];
        // console.log("tok :", token);
        if (!token) {
            return false
        }

        let decoded;

        try {
            decoded = jwt.verify(token, config.tokenSecret);
        } catch (err) {
            decoded = null;
            console.error(err);
        }

        if (decoded) {
            return decoded.id
        } else {
            return false;
        }
    } catch (err) {
        return false;
    }
};

export default isAuthenticated;