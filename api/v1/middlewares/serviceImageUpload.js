import multer from 'multer';
import cloudinary from '../../../providers/cloudinary';
import cloudinaryStorage from 'multer-storage-cloudinary';

const storage = cloudinaryStorage({
    cloudinary,
    folder: "service-image",
    allowedFormats: ["jpg", "png"],
    transformation: [{
        width: 500,
        height: 500,
        crop: "limit"
    }]
});

const uploader = multer({
    storage: storage
});

export default uploader;