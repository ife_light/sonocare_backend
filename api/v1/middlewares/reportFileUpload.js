import multer from 'multer';
import azureMulterSrorage from '../../../providers/azureMulterStorage';

const storage = azureMulterSrorage('report-files');

const uploader = multer({
    storage
});

export default uploader;