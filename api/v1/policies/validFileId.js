import File from '../../../models/File';

import errorResponse from '../responses/errorResponse';
import response505 from '../responses/505';

/* eslint-disable-next-line */
const validFileId = async function validFileIdCheck(req, res, next) {
    try {
        const { fileId } = req.params;

        const validFileId = await File.findById(fileId).exec();
        if (!validFileId) {
            return errorResponse(res, 400, "The File with that ID does not exist");
        }
        next();
    } catch (err) {
        console.error(err);
        return response505(res);
    }
   };
   
   export default validFileId;
   