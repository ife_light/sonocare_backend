import Report from '../../../models/Report';

import errorResponse from '../responses/errorResponse';
import response505 from '../responses/505';

/* eslint-disable-next-line */
const validReportId = async function validReportIdCheck(req, res, next) {
    try {
        const { reportId } = req.params;

        const validReportId = await Report.findById(reportId).exec();
        if (!validReportId) {
            return errorResponse(res, 400, "The report with that ID does not exist");
        }
        next();
    } catch (err) {
        console.error(err);
        return response505(res);
    }
   };
   
   export default validReportId;
   