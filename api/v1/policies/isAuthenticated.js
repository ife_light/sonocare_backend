import jwt from 'jsonwebtoken';
import config from '../../../config/default';

/* eslint-disable-next-line */
const isAuthenticated = function authenticationCheck(req, res, next) {
 try {
  const token = req.headers['x-access-token'];
  // console.log("tok :", token);
  if (!token) {
    return res.status(401).send({
      auth: false,
      token: null,
      message: 'No token provided.',
    });
  }

  let decoded;

  try {
    decoded = jwt.verify(token, config.tokenSecret);
  } catch (err) {
    decoded = null;
    console.error(err);
  }

  if (decoded) {
    // console.log(decoded);
    req.app.set('userId', decoded.id); // controllers depend on this
    next();
  } else {
    return res.status(401).json({
      auth: false,
      token: null,
      message: 'Failed to authenticate token.',
    });
  }
 } catch (err) {
   console.error(err);
  return res.status(401).json({
    auth: false,
    token: null,
    message: 'Invalid token.',
  });
 }
};

export default isAuthenticated;
