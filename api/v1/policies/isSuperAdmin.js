import config from '../../../config/default';

import response505 from '../responses/505';
import errorResponse from '../responses/errorResponse';
import userResponse from '../responses/userResponse';
import successResponse from '../responses/successResponse';

import User from '../../../models/User';

/**
 * Is admin check Middleware/policy
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
const isSuperAdmin = async function isSuperAdmincheck (req, res, next) {
    try {
        const userId = req.app.get('userId');
        const user = await User.findOne({
            _id: userId
        })

        if (user && user.isAdmin && user.isSuperAdmin) {
            return next();
        }

        return errorResponse(res, 403, "You are forbidden, not a Super Administrator")
        
    } catch (err) {
        console.error(err);
        return response505(res)
    }
}

export default isSuperAdmin;