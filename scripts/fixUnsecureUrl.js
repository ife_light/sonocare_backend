/**
 * This was written to change Cloudinary picture unsecure urls to secure urls
 */

 const mongoose  = require('mongoose');

const Types = mongoose.Schema.Types;

const serviceSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	picture: {
		type: String,
		default: null
	}
}, {
	timestamps: true
})


const Service = mongoose.model('Service', serviceSchema);

const subServiceSchema = new mongoose.Schema({

	picture: {
		type: String,
		default: null
    },
    amount: {
        type: Number,
        default: 0,
	},
	country: {
		type: String,
		default: "Nigeria"
	},
	parent: {
		type: Types.ObjectId,
		default: null
	},
    service: {
        type: Types.ObjectId,
		ref: 'Service',
		required: true
	},
	homeService: {
		type: Boolean,
		default: false,
	},
	children: [{
		type: Types.ObjectId,
		ref: 'SubService'
	}]
}, {
	timestamps: true
})


const SubService =  mongoose.model('SubService', subServiceSchema)

const adminSchema = new mongoose.Schema({
	firstName: {
		type : String
	},
	lastName: {
		type : String
	},
	email: {
		type : String
	},
	password: {
		type : String
	},
	passwordCode : {
		code: {type : Number},
		expiresIn : {type : Date}
	},
	isActive: {
		type: Boolean, 
		default: false
	},
	isDeleted: {
		type: Boolean, 
		default: false
	},
	profilePicUrl : {
		type : String
	},
	cloudinaryImagePublicId: {
		type : String
	},
}, {
	timestamps: true
})


const Admin = mongoose.model('Admin', adminSchema);


const userSchema = new mongoose.Schema({
	firstName: {
		type : String
	},
	lastName: {
		type : String
	},
	email: {
		required: true,
		type : String
	},
	phone: { //Mobile Phone Number
		type : String
	},
	password: {
		type : String
	},
	type: { //Wether partner or patient or admin
		type : String,
		required: true,
	},
	address: { //Residential Address
		type : String
	},
	institutionAddress : { //The institution address
		type : String
	},
	sex: {
		type : String
	},
	dateOfBirth: {
		type : String
	},
	institutionName: {
		type : String
	},
	institutionType: {
		type : String
	},
	incomeRange : {
		min : {
			type: Number
		},
		max : {
			type : Number
		},
		currency : { //The currency of the income range
			type : String
		}
	},
	isAdmin: { //Admin check
		type: Boolean,
	},
	superAdmin: { //Admin check
		type: Boolean,
	},
	passwordCode : {
		code: {type : Number},
		expiresIn : {type : Date}
	},
	active: {
		type: Boolean, 
		default: false
	},
	isDeleted: {
		type: Boolean, 
		default: false
	},
	profilePicUrl : {
		type : String
	},
	cloudinaryImagePublicId: {
		type : String
	},
	location: {
		type: { type: String, default: "Point", enum: ["Point"] },
		coordinates: {
			type: [Number],
			default: [0, 0],
		  }
	},
	subscribers: [{
		type: Types.ObjectId,
		ref: 'User'
	}],
	services: [{
		type: Types.ObjectId,
		ref: 'Service'
	}],
	subServices: [{
		type: Types.ObjectId,
		ref: 'SubService'
	}],
	fcmNotificationId: {
		type : String, //This is the notification registration ID for Firebase Cloud Messaging
	}
}, {
	timestamps: true
})


const User = mongoose.model('User', userSchema);



mongoose.connect('mongodb://localhost/sonocaredev', {
    autoIndex: false
});

mongoose.connection.on("connected", function () {
    console.log(" Mongo Database Connected");
    /**
     * The task Code
     */
    (async () => {
        /**
         * For the Users Model
         */
        console.log('Fetching users');
        const users = await User.find({}).exec();
        console.log('Updating Users');
        users.forEach(async (user) => {
            const isPic = user.profilePicUrl;
            if (isPic) {
                const picUrl = user.profilePicUrl;
                const protocol = picUrl.split('res');
                if (protocol[0] && protocol[0] != 'https://') {
                    console.log("Working on " + user.profilePicUrl);
                    const newUpdate = await User.findByIdAndUpdate(user._id, {
                        profilePicUrl: 'https://' + 'res' + protocol[1]
                    })
                    console.log("Updated to " + 'https://' + 'res' + protocol[1]);
                }
            }
        })

        /**
         * For the Admin Model
         */
        console.log('Fetching Admin');
        const admins = await Admin.find({}).exec();
        console.log('Working on  Admins');
        admins.forEach(async (admin) => {
            const isPic = admin.profilePicUrl;
            if (isPic) {
                const picUrl = admin.profilePicUrl;
                const protocol = picUrl.split('res');
                if (protocol[0] && protocol[0] != 'https://') {
                    console.log("Working on " + admin.profilePicUrl);
                    const newUpdate = await Admin.findByIdAndUpdate(admin._id, {
                        profilePicUrl: 'https://' + 'res' + protocol[1]
                    })
                    console.log("Updated to " + 'https://' + 'res' + protocol[1]);
                }
            }
        })


         /**
          * For Service Model
          */
         console.log('Fetching Services');
        const services = await Service.find({}).exec();
        console.log('Working on  Services');
        services.forEach(async (service) => {
            const isPic = service.picture;
            if (isPic) {
                const picUrl = service.picture;
                const protocol = picUrl.split('res');
                if (protocol[0] && protocol[0] != 'https://') {
                    console.log("Working on " + service.picture);
                    const newUpdate = await Service.findByIdAndUpdate(service._id, {
                        picture: 'https://' + 'res' + protocol[1]
                    })
                    console.log("Updated to " + 'https://' + 'res' + protocol[1]);
                }
            }
        })


          /**
           * For Sub servie Model
           */
          console.log('Fetching Sub Services');
        const subservices = await SubService.find({}).exec();
        console.log('Working on  subservices');
        subservices.forEach(async (service) => {
            const isPic = service.picture;
            if (isPic) {
                const picUrl = service.picture;
                const protocol = picUrl.split('res');
                if (protocol[0] && protocol[0] != 'https://') {
                    console.log("Working on " + service.picture);
                    const newUpdate = await SubService.findByIdAndUpdate(service._id, {
                        picture: 'https://' + 'res' + protocol[1]
                    })
                    console.log("Updated to " + 'https://' + 'res' + protocol[1]);
                }
            }
        })
    })()
})

mongoose.connection.on("disconnected", function () {
    console.log(" Mongo Database Disconnected...");
    console.log("Exiting, because of mongoose no connection");
    process.exit(0);
})

mongoose.connection.on("error", function (err) {
    console.log("An error occured");
    console.log(err);
    process.exit();
})

process.on("SIGINT", function () {
    console.log(" Closing Mongo Database ....");
    mongoose.connection.close(function (err) {
        if (!err) {
            console.log("Mongo Database now Closed");
            process.exit(0);
        }
    })
})


