import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';

import api from './api/index'; 

//Requiring Mongoose database Connection
require('./providers/mongoConnection');

//Reminder cron
require('./cron/appointmentReminder');

// Initiating Main Express app
const app = express();

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Using logger to log requests to the console
app.use(logger('dev'));


app.get('/', (req, res) => {
  res.send('Page working yes');
});

// Api Router
app.use('/api', api);

/**
 * 404 page
 */
app.use((req, res) => {
    res.status(404).send('Page not found');
  });
  
export default app;
  