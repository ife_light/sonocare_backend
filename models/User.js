import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

import codeGenerator from '../services/codeGenerator'

const Types = mongoose.Schema.Types;

const userSchema = new mongoose.Schema({
	firstName: {
		type : String
	},
	lastName: {
		type : String
	},
	email: {
		required: true,
		type : String
	},
	phone: { //Mobile Phone Number
		type : String
	},
	password: {
		type : String
	},
	type: { //Wether partner or patient or admin
		type : String,
		required: true,
	},
	address: { //Residential Address
		type : String
	},
	institutionAddress : { //The institution address
		type : String
	},
	sex: {
		type : String
	},
	dateOfBirth: {
		type : String
	},
	institutionName: {
		type : String
	},
	institutionType: {
		type : String
	},
	incomeRange : {
		min : {
			type: Number
		},
		max : {
			type : Number
		},
		currency : { //The currency of the income range
			type : String
		}
	},
	isAdmin: { //Admin check
		type: Boolean,
	},
	superAdmin: { //Admin check
		type: Boolean,
	},
	passwordCode : {
		code: {type : Number},
		expiresIn : {type : Date}
	},
	active: {
		type: Boolean, 
		default: false
	},
	isDeleted: {
		type: Boolean, 
		default: false
	},
	profilePicUrl : {
		type : String
	},
	cloudinaryImagePublicId: {
		type : String
	},
	location: {
		type: { type: String, default: "Point", enum: ["Point"] },
		coordinates: {
			type: [Number],
			default: [0, 0],
		  }
	},
	subscribers: [{
		type: Types.ObjectId,
		ref: 'User'
	}],
	services: [{
		type: Types.ObjectId,
		ref: 'Service'
	}],
	subServices: [{
		type: Types.ObjectId,
		ref: 'SubService'
	}],
	fcmNotificationId: {
		type : String, //This is the notification registration ID for Firebase Cloud Messaging
	}
}, {
	timestamps: true
})

//location Index
userSchema.index({ location: '2dsphere'});


// methods ======================
// generating a hash
userSchema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//Hash the password and update to new password
userSchema.methods.createPassword = async function (password) {
	const newPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	const status = await this.model('User').updateOne({
		_id : this._id
	}, {
		$set : {
			password : newPassword
		}
	}).exec();

	return this;
}

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

//This is used to generate a password code
//for change of password
userSchema.methods.generatePasswordCode = async function () {
	const generatedCode = codeGenerator(5);
	//Creating an expiring date of One day
	let expiringTime  = new Date();
    expiringTime.setHours(expiringTime.getHours() + 24); //24 is the hours in a day
	const status = await this.model('User').updateOne({
		_id : this._id
	}, {
		$set : {
			"passwordCode.code" : generatedCode,
			"passwordCode.expiresIn" : expiringTime
		}
	}).exec();
	return {
		code: generatedCode,
		expiringTime
	}
}

//This is used to check whether the password code matches
userSchema.methods.checkPasswordCodeMatch = function (code) {
	return Number(code) === this.passwordCode.code;
}

//This is used to check whether the password code has expired
userSchema.methods.isPasswordCodeExpired = function () {
	return this.passwordCode.expiresIn.getTime() >= Date.now();
}

//Expire the code after usage
userSchema.methods.expireCode = function () {  
	let expiringTime  = new Date();
    expiringTime.setHours(expiringTime.getHours() - 300); //Force expiring the code
	return this.model('User').updateOne({
		_id : this._id
	}, {
		$set : {
			"passwordCode.expiresIn" : expiringTime
		}
	}).exec()
}

userSchema.methods.updateFCMRegistrationId = async function (registrationId) {
	const status = await this.model('User').updateOne({
		_id : this._id
	}, {
		$set : {
			fcmNotificationId : registrationId
		}
	}).exec();

	return this;
}

///============ Admin Section ============================


const User = mongoose.model('User', userSchema);
//Ensure index are created
User.ensureIndexes((err) => {
	if (err) {
		console.error(err);
	}
})
export default User;
