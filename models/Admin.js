import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

import codeGenerator from '../services/codeGenerator'

const Types = mongoose.Schema.Types;

const adminSchema = new mongoose.Schema({
	firstName: {
		type : String
	},
	lastName: {
		type : String
	},
	email: {
		type : String
	},
	password: {
		type : String
	},
	passwordCode : {
		code: {type : Number},
		expiresIn : {type : Date}
	},
	isActive: {
		type: Boolean, 
		default: false
	},
	isDeleted: {
		type: Boolean, 
		default: false
	},
	profilePicUrl : {
		type : String
	},
	cloudinaryImagePublicId: {
		type : String
	},
}, {
	timestamps: true
})



// methods ======================
// generating a hash
adminSchema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

//Hash the password and update to new password
adminSchema.methods.createPassword = async function (password) {
	const newPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
	const status = await this.model('Admin').updateOne({
		_id : this._id
	}, {
		$set : {
			password : newPassword
		}
	}).exec();

	return this;
}

// checking if password is valid
adminSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

//This is used to generate a password code
//for change of password
adminSchema.methods.generatePasswordCode = async function () {
	const generatedCode = codeGenerator(5);
	//Creating an expiring date of One day
	let expiringTime  = new Date();
    expiringTime.setHours(expiringTime.getHours() + 24); //24 is the hours in a day
	const status = await this.model('Admin').updateOne({
		_id : this._id
	}, {
		$set : {
			"passwordCode.code" : generatedCode,
			"passwordCode.expiresIn" : expiringTime
		}
	}).exec();
	return {
		code: generatedCode,
		expiringTime
	}
}

//This is used to check whether the password code matches
adminSchema.methods.checkPasswordCodeMatch = function (code) {
	return Number(code) === this.passwordCode.code;
}

//This is used to check whether the password code has expired
adminSchema.methods.isPasswordCodeExpired = function () {
	return this.passwordCode.expiresIn.getTime() >= Date.now();
}

//Expire the code after usage
adminSchema.methods.expireCode = function () {  
	let expiringTime  = new Date();
    expiringTime.setHours(expiringTime.getHours() - 300); //Force expiring the code
	return this.model('Admin').updateOne({
		_id : this._id
	}, {
		$set : {
			"passwordCode.expiresIn" : expiringTime
		}
	}).exec()
}

export default mongoose.model('Admin', adminSchema);
