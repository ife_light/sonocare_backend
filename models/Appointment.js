import mongoose, { mongo } from 'mongoose';

const Types = mongoose.Schema.Types;

const appointmentSchema = new mongoose.Schema({
	partnerId: {
		type: mongo.ObjectID, 
		required: true,
		ref: 'User'
	},
	patientId: {
		type: Types.ObjectId, 
		required: true,
		ref: 'User'
	},
	description: {
		type: String,
		required: true
	},
	patientScheduledDate: {
		type: Date, 
		default: Date.now, 
		required: true 
	},
	partnerAcceptedDate: {
		type: Date,
	},
	partnerAccepted: {
		type: Boolean,
		default: false
	},
	partnerRejectedDate: {
		type: Date,
	},
	partnerRejected: {
		type: Boolean,
		default: false
	},
	partnerRescheduledDate: {
		type: Date,
	},
	partnerRescheduled: {
		type: Boolean,
		default: false
	},
	partnerRescheduledReason : {
		type: String,
	},
	partnerRescheduledTo: {
		type: Date,
	},
	service: {
		type: Types.ObjectId, 
		required: true,
		ref: 'Service'
	},
	subService: {
		type: Types.ObjectId, 
		ref: 'SubService',
		default: null
	},
	patientAcceptRescheduledDate: {
		type: Date
	},
	patientAcceptRescheduled: {
		type: Boolean
	},
	patientDeclineRescheduledDate: {
		type: Date
	},
	patientDeclineRescheduled: {
		type: Boolean
	},
	appointmentDate: {
		type: Date,
	},
	lastRemindedDate: {
		type: Date,
	},
	status: {
		//the status of the appointment
		//can be 'patient-initiated', 'partner-accepted', 'partner-declined', 'partner-rescheduled'
		//and also 'patient-accepted-reschedule', 'patient-declined-reschedule'
		type: String 
	},
}, {
	timestamps: true
})

export default mongoose.model('Appointment', appointmentSchema)