import mongoose from 'mongoose';

const Types = mongoose.Schema.Types;

const fileSchema = new mongoose.Schema({
	fieldname: {
		type: String,
	},
	type: {
        type: String,
    },
    url: {
        type: String,
        required: true,
    },
    user : {
        type: Types.ObjectId,
        ref: 'User',
    },
    size: {
        type: String,
    },
    blob: {
        type: String,
    },
    azureContainer: {
        type: String,
    }
    
}, {
	timestamps: true
})


export default mongoose.model('File', fileSchema)