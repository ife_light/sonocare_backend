import fs from 'fs';
import path from 'path';
import mongoose from 'mongoose';

import dbConfig from '../config/db'
import config from '../config/default'


const basename = path.basename(__filename);
const models = {};

const dbEnvConfig = dbConfig[config.env];

if(dbEnvConfig.dbHost != ''){
    var files = fs
      .readdirSync(__dirname)
      .filter((file) => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
      .forEach((file) => {
        var filename = file.split('.')[0];
        var model_name = filename.charAt(0).toUpperCase() + filename.slice(1);
        models[model_name] = require('./'+file);
    });

    mongoose.Promise = global.Promise; //set mongo up to use promises
    const dbAuth = dbEnvConfig.dbUser && dbEnvConfig.dbPassword ? dbEnvConfig.dbUser + dbEnvConfig.dbPassword  + '@': '';
    const mongo_location = 'mongodb://' + dbAuth + dbEnvConfig.dbHost+':'+dbEnvConfig.dbPort+'/'+dbEnvConfig.dbName;

    mongoose.connect(mongo_location).catch((err)=>{
        console.log('*** Can Not Connect to Mongo Server:', mongo_location)
    })

    let db = mongoose.connection;
    module.exports = db;
    db.once('open', ()=>{
        console.log('Connected to mongo at '+mongo_location);
    })
    db.on('error', (error)=>{
        console.log("error", error);
    })
    // End of Mongoose Setup
    }else{
        console.log("No Mongo Credentials Given");
        process.exit();
    }