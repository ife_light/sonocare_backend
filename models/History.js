import mongoose from 'mongoose';

const Types = mongoose.Schema.Types;

const historySchema = new mongoose.Schema({
    type: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        required: true,
    },
    partner: {
        type: Types.ObjectId,
        ref: 'User',
    },
    patient: {
        type: Types.ObjectId,
        ref: 'User',
    },
    appointment: {
        type: Types.ObjectId,
        ref: 'Appointment',
        default: null
    },
    date: {
        type: Date,
        default: Date.now
    }
}, {
	timestamps: true
})


export default mongoose.model('History', historySchema)