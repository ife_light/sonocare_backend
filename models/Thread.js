import mongoose from 'mongoose'

const threadSchema = new mongoose.Schema({
	senderId: {type: Types.ObjectId, required: true},
	receiverId: {type: Types.ObjectId, required: true},
	isDeleted: {type: Boolean, default: false}
}, {
	timestamps: true
})

export default mongoose.model('Thread', threadSchema)
