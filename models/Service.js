import mongoose from 'mongoose';

const Types = mongoose.Schema.Types;

const serviceSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	picture: {
		type: String,
		default: null
	}
}, {
	timestamps: true
})


export default mongoose.model('Service', serviceSchema)