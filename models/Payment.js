import mongoose from 'mongoose'

const paymentSchema = mongoose.Schema({
	reference: String,
	userId: {type: Types.ObjectId, required: true},
	amount: Number,
	date: {type: Date, default: Date.now, required: true },
	isDeleted: {type: Boolean, default: false}
}, {
	timestamps: true
})

export default mongoose.model('Payment', paymentSchema)
