import mongoose from 'mongoose';

const Types = mongoose.Schema.Types;

const reportSchema = new mongoose.Schema({
	title: {
		type: String,
		required: true
	},
	description: {
        type: String,
        required: true
    },
    type: {
        type: String,
    },
    category: {
        type: String,
    },
	files: [{
        type: Types.ObjectId,
		ref: 'File',
    }], 
    partner: {
        type: Types.ObjectId,
        ref: 'User',
    },
    patient: {
        type: Types.ObjectId,
        ref: 'User',
    },
    appointment: {
        type: Types.ObjectId,
        ref: 'Appointment'
    },
}, {
	timestamps: true
})


export default mongoose.model('Report', reportSchema)