import mongoose from 'mongoose'

const messageSchema = mongoose.Schema({
	threadId: {type: Types.ObjectId, required: true},
	senderId: {type: Types.ObjectId, required: true},
	message: String,
	isDeleted: {type: Boolean, default: false}
}, {
	timestamps: true
})

export default mongoose.model('Message', messageSchema)
