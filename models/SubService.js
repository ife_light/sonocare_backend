import mongoose from 'mongoose';

const Types = mongoose.Schema.Types;

const subServiceSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	picture: {
		type: String,
		default: null
    },
    amount: {
        type: Number,
        default: 0,
	},
	country: {
		type: String,
		default: "Nigeria"
	},
	parent: {
		type: Types.ObjectId,
		default: null
	},
    service: {
        type: Types.ObjectId,
		ref: 'Service',
		required: true
	},
	homeService: {
		type: Boolean,
		default: false,
	},
	children: [{
		type: Types.ObjectId,
		ref: 'SubService'
	}]
}, {
	timestamps: true
})


export default mongoose.model('SubService', subServiceSchema)