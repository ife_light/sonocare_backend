import MulterAzureStorage from 'multer-azure-storage';

import config from '../config/default'


const accountName = config.azure.storageAccount;
const accountKey = config.azure.storageKey;

export default (containerName) => {
    return new MulterAzureStorage({
        azureStorageConnectionString: `DefaultEndpointsProtocol=https;AccountName=${accountName};AccountKey=${accountKey};EndpointSuffix=core.windows.net`,
        containerName,
        containerSecurity: 'blob'
      })
}