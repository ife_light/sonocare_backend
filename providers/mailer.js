import path from 'path';
import defaultConfig from '../config/default';

var api_key = defaultConfig.sendgridApiKey;

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(api_key);

/**
 * This is the mailer for sending mails
 * @param subject {String} - The subject of the mail
 * @param to {String} - The email of the reciever
 * @param text {String} - The email format in text form
 * @param html {String} - The email format in HTML form
 * @returns {Promise} - It returns a Promise object
 */
export default (subject="Sonocare", to, text, html) => {
  
  var msg = {
    from: `Sonocare App Service <postmaster@sonocare.ng>`,
    to: to,
    subject: subject,
    text,
    html
  };

  return new Promise((resolve, reject) => {
    try {
      const res = sgMail.send(msg);
      resolve(true)
    } catch (err) {
      console.log(err.response.body);
      reject(err)
    }
  });
  
  
}