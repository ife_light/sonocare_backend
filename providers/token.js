import jwt from 'jsonwebtoken';
import config from '../config/default';

/**
 * For signing user's ID
 * @param id {string} - the usersID
 * @returns String - returns the token in string format
 */
export function signID  (id)  {
    return jwt.sign({ id }, config.tokenSecret, { expiresIn: config.tokenExpiresIn });
}

/**
 * For verifying user's token from ID
 * @param token {string} - The user supplied token
 * @returns Object - A decoded object when valid otherwise null
 */
export function verifyID (token) { 
    try {
        return jwt.verify(token, config.tokenSecret);
    } catch (err) {
        console.log(err);
        return false;
    }
}