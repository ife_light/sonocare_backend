import dotenv from 'dotenv';
import mongoose from 'mongoose';

import dbConfig from '../config/db'
import config from '../config/default'

dotenv.config();

const options = {
    keepAlive: true,
    keepAliveInitialDelay: 300000,
    autoIndex: false,
}

const dbEnvConfig = dbConfig[config.env];

if (dbEnvConfig.dbUser && dbEnvConfig.dbPassword) {
    options.user = dbEnvConfig.dbUser;
    options.pass = dbEnvConfig.dbPassword;
    options.authSource = 'admin';
}

//const dbAuth = dbEnvConfig.dbUser && dbEnvConfig.dbPassword ? dbEnvConfig.dbUser + dbEnvConfig.dbPassword  + '@': '';
const mongo_location = 'mongodb://' + dbEnvConfig.dbHost + ':'+ dbEnvConfig.dbPort + '/' + dbEnvConfig.dbName;

//console.log(config.env);

mongoose.connect(mongo_location, options);

mongoose.connection.on("connected", function () {
    console.log(" Mongo Database Connected");
})

mongoose.connection.on("disconnected", function () {
    console.log(" Mongo Database Disconnected...");
    console.log("Exiting, because of mongoose no connection");
    process.exit(0);
})

mongoose.connection.on("error", function (err) {
    console.log("An error occured");
    console.log(err);
    process.exit();
})

process.on("SIGINT", function () {
    console.log(" Closing Mongo Database ....");
    mongoose.connection.close(function (err) {
        if (!err) {
            console.log("Mongo Database now Closed");
            process.exit(0);
        }
    })
})