import FCM from 'fcm-node';

import config from '../config/default';

const serverKey = config.fcmServerKey;

const fcm = new FCM(serverKey);

export default fcm;