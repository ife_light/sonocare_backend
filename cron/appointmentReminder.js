import {
    queue
} from 'async';
import {
    scheduleJob
} from 'node-schedule';

import Appointment from '../models/Appointment';

import appointmentReminderMail from '../services/notificationMails/appointmentReminder'

async function reminderCron () {
    try {
        const presentDate = new Date(Date.now());
        const next50mins = new Date();
        next50mins.setMinutes(next50mins.getMinutes() + 50 );
        const next60mins = new Date();
        next60mins.setMinutes(next60mins.getMinutes() + 61 )


        const appointments = await Appointment.find({
            $and: [
                {appointmentDate: {$exits: true}}, 
                {appointmentDate: {$ne: null}}, 
                {appointmentDate: {$ne: ''}},
                {appointmentDate: {$gt: next50mins}},
                {appointmentDate: {$lt: next60mins}}
            ] 
        })
        .populate('partnerId')
        .populate('patientId')
        .exec()
    
        const q = queue( (appointment, cb) => {
            appointmentReminderMail(appointment);
            const appointmentDetails = Appointment.findByIdAndUpdate(appointment._id, {
                lastRemindedDate: new Date()
            });
            cb();
        }, 5);

        q.push(appointments)
    } catch (err) {
        console.error(err)
    }
}

scheduleJob('*/10 * * * *', reminderCron);