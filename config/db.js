import dotenv from 'dotenv';

dotenv.config()

/**
 * Database configurations
 */
export default {
    //production Configurations
    production : {
        dbName: process.env.DB_NAME || 'sonocare',
        dbUser: process.env.DB_USER || '',
        dbPassword: process.env.DB_PASSWORD || '',
        dbHost: process.env.DB_HOST || 'localhost',
        dbPort: process.env.DB_PORT || '27017'
    },
    development : {
        dbName: 'sonocaredev',
        dbUser: '',
        dbPassword: '',
        dbHost: 'localhost',
        dbPort:  '27017'
    },
    test : {
        dbName: 'sonocaretest',
        dbUser: '',
        dbPassword: '',
        dbHost: 'localhost',
        dbPort:  '27017'
    }
}