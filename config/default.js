import dotenv from 'dotenv';

dotenv.config();

const { NODE_ENV } = process.env

console.log(`${NODE_ENV} is the node environment`);

const defaultConfig = {
  tokenSecret: process.env.TOKEN_SECRET || 'dhkmsgseieYhb79Nj',
  env: NODE_ENV || 'development',
  tokenExpiresIn: process.env.TOKEN_EXPIRES_IN || 86400,
  sendgridApiKey: process.env.SENDGRID_API_KEY,
  cloudinary: {
    apiKey : process.env.CLOUDINARY_API_KEY,
    apiSecret: process.env.CLOUDINARY_API_SECRET,
    cloudName: process.env.CLOUDINARY_CLOUD_NAME,
  },
  fcmServerKey: process.env.FCM_SERVER_KEY,
  azure: {
    storageAccount: process.env.AZURE_STORAGE_ACCOUNT,
    storageKey: process.env.AZURE_STORAGE_KEY
  },
  companyMail: 'info@sonocare.ng',
  companyPhoneNumber: '+234(0)703 4199 466'
}

export default defaultConfig;