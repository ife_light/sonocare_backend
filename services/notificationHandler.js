import fcm from '../providers/FCM';

import appointmentInitiatedMail from './notificationMails/appointmentInitiated';
import partnerAcceptedMail from './notificationMails/partnerAccepted';
import partnerRejectedMail from './notificationMails/partnerRejected';
import partnerRescheduleMail from './notificationMails/partnerReschedule';
import patientAcceptRescheduleMail from './notificationMails/patientAcceptReschedule';
import patientRejectRescheduleMail from './notificationMails/patientRejectReschedule';
import newReportMail from './notificationMails/newReport';

const notificationHandler = async ({patient, partner, appointment, type, data, to, eventType, fcmNotificationId}) => {
    /**
     * The Notification Events
     * The notification events are events from the Appointments
     * Which are:
     *      A Patient first of all initiates an appointment. [Appointment Staus: 'patient-initiated']
            The partner accepts the appointment. [Appointment Staus: 'partner-accepted']
            The partner declines the appointment. [Appointment Staus: 'partner-declined']
            The partner reschedules the appointment. [Appointment Staus: 'partner-rescheduled']
            The patient accepts the rescheduled appointment. [Appointment Staus: 'patient-accepted-reschedule']
            The patient declines the rescheduled appointment. [Appointment Staus:'patient-declined-reschedule']
     */

     if (type && type === 'appointment' && eventType === 'patient-initiated') {
         // Notifications for when an appointment is initiated

        //FCM Notification
        //Android notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'A new appointment request',
                body: 'A patient has requested to book an appointment with you.'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })

        //Email Notification...
        appointmentInitiatedMail({patient, partner, appointment})
     }

     if (type && type === 'appointment' && eventType === 'partner-accepted') {
        // Notifications for when an appointment is partner-accepted

        //FCM Notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'Appointment Accepted',
                body: 'A partner you booked an appointment with has accepted your request'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
        //Email Notification
        partnerAcceptedMail({patient, partner, appointment});
    }

    if (type && type === 'appointment' && eventType === 'partner-declined') {
        // Notifications for when an appointment is partner-declined
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'Appointment Rejected',
                body: 'A partner you booked an appointment with has rejected your request'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
        //Email Notification
        partnerRejectedMail({patient, partner, appointment});
    }

    if (type && type === 'appointment' && eventType === 'partner-rescheduled') {
        // Notifications for when an appointment is partner-rescheduled
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'Appointment Rescheduled',
                body: 'A partner you booked an appointment with has rescheduled your appointment'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
        //Email Notification
        partnerRescheduleMail({patient, partner, appointment});
    }

    if (type && type === 'appointment' && eventType === 'patient-accepted-reschedule') {
        // Notifications for when an appointment is patient-accepted-reschedule
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'Appointment Reschedule Accepted',
                body: 'An appointment you rescheduled has been accepted by the patient'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
        //Email Notification
        patientAcceptRescheduleMail({patient, partner, appointment});
    }

    if (type && type === 'appointment' && eventType === 'patient-declined-reschedule') {
        // Notifications for when an appointment is patient-declined-reschedule
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            notification: {
                title: 'Appointment Reschedule Rejected',
                body: 'An appointment you rescheduled has been rejected by the patient'
            },
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
        //Email Notification
        patientRejectRescheduleMail({patient, partner, appointment});
    }

    /***
     * FCM Notification to handle the Consultation call API
     */
    if (type && type === 'consultation' && eventType === 'initiate-call') {
        // Notifications for when a user makes a call
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
    }

    if (type && type === 'consultation' && eventType === 'reject-call') {
        // Notifications for when a user rejects a call
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
    }

    if (type && type === 'consultation' && eventType === 'accept-call') {
        // Notifications for when a user rejects a call
        //FCM Notification
        const message = {
            to: fcmNotificationId,
            data 
        }

        fcm.send(message, (err, response) => {
            if (err) {
                console.error(err)
            } else {
                console.log(response);
            }
        })
    }

    //// Notifications for when a Report is created
    if (type && type === 'report' && eventType === 'new-report') {
        //FCM Notification
       const message = {
           to: fcmNotificationId,
           notification: {
               title: 'A new report',
               body: 'You have a new report'
           },
           data 
       }

       fcm.send(message, (err, response) => {
           if (err) {
               console.error(err)
           } else {
               console.log(response);
           }
       })
       //Email Notification
       newReportMail({patient, partner});
    }
}


export default notificationHandler;