import User from '../models/User';

/**
 * To get the profile Picture of a user
 * @param {String} id The ID of the user
 * @returns {Promise<String>} - The image Link is returned
 */
const getProfilePic = async function getProfilePicture (id) {
    const user = await User.findOne({_id: id}).exec();
    const profilePicLink = user.profilePicUrl;
    return profilePicLink;
}

export default getProfilePic;