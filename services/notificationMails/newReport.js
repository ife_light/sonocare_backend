import mail from '../../providers/mailer';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

import config from '../../config/default'

const {companyMail, companyPhoneNumber} = config;

export default ({patient, partner}) => {

    const capitalizePatientName = capitalize(patient.firstName);
    const capitalizePartnerName = capitalize(partner.institutionName);
    const text = `
Dear ${capitalizePatientName},
    
We are notifying you that ${capitalizePartnerName} you booked an appointment with, has provided a report of the appointment.. 
    
If you have any questions or more enquiries, don’t hesitate to call Sonocare on  ${companyPhoneNumber} or send an email to ${companyMail}. Have a nice day!
    
Best,
    
Sonocare HealthCare`;
    const subject= `${capitalizePartnerName} just Provided a Report of an Appointment`;
    const to = patient.email;
    mail(subject, to, text, text)
}
