import mail from '../../providers/mailer';
import moment from 'moment';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

import config from '../../config/default'

const {companyMail, companyPhoneNumber} = config;

export default ({patient, partner, appointment}) => {

    const capitalizePatientName = capitalize(patient.firstName);
    const capitalizePartnerName = capitalize(partner.institutionName);
    const appointmentTime = moment(appointment.patientScheduledDate).format("YYYY-MM-DD hh:mm A")
    const text = `
Dear ${capitalizePartnerName},
    
We are notifying you that a patient want to book an appointment with your organisation at ${appointmentTime}, kindly visit your dashboard to take appropriate actions. 
    
If you have any questions or more enquiries, don’t hesitate to call Sonocare on  ${companyPhoneNumber} or send an email to ${companyMail}. Have a nice day!
    
Best,
    
Sonocare HealthCare`;
    const subject= `${capitalizePatientName} want to book an Appointment with you.`;
    const to = partner.email;
    mail(subject, to, text, text)
}