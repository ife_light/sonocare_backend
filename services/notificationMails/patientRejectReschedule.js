import mail from '../../providers/mailer';
import moment from 'moment';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

import config from '../../config/default'

const {companyMail, companyPhoneNumber} = config;

export default ({patient, partner, appointment}) => {

    const capitalizePatientName = capitalize(patient.firstName);
    const capitalizePartnerName = capitalize(partner.institutionName);
    const appointmentTime = moment(appointment.patientScheduledDate).format("YYYY-MM-DD hh:mm A");
    const rescheduledTime = moment(appointmentTime.partnerRescheduledDate).format("YYYY-MM-DD hh:mm A");
    const text = `
Dear ${capitalizePartnerName},
    
We are notifying you that the appointment you reschedule for ${rescheduledTime} has been rejected by ${capitalizePatientName}.
    
If you have any questions or more enquiries, don’t hesitate to call Sonocare on  ${companyPhoneNumber} or send an email to ${companyMail}. Have a nice day!
    
Best,
    
Sonocare HealthCare`;
    const subject= `Your Rescheduled appointment with ${capitalizePatientName} has been Rejected`;
    const to = partner.email;
    mail(subject, to, text, text)
}
