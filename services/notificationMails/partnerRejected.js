import mail from '../../providers/mailer';
import moment from 'moment';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

import config from '../../config/default'

const {companyMail, companyPhoneNumber} = config;

export default ({patient, partner, appointment}) => {

    const capitalizePatientName = capitalize(patient.firstName);
    const capitalizePartnerName = capitalize(partner.institutionName);
    const appointmentTime = moment(appointment.patientScheduledDate).format("YYYY-MM-DD hh:mm A")
    const text = `
Dear ${capitalizePatientName},
    
We are notifying you that your appointment you booked with ${capitalizePartnerName} to be scheduled on ${appointmentTime} has been rejected.
    
If you have any questions or more enquiries, don’t hesitate to call Sonocare on  ${companyPhoneNumber} or send an email to ${companyMail}. Have a nice day!
    
Best,
    
Sonocare HealthCare`;
    const subject= `Your appointment with ${capitalizePartnerName} has been Accepted`;
    const to = patient.email;
    mail(subject, to, text, text)
}
