import mail from '../../providers/mailer';
import moment from 'moment';

const capitalize = (s) => {
    if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

import config from '../../config/default'

const {companyMail, companyPhoneNumber} = config;

export default (appointment) => {

    const capitalizePatientName = capitalize(appointment.patientId.firstName);
    const capitalizePartnerName = capitalize(appointment.partnerId.institutionName);
    const appointmentTime = moment(appointment.appointmentDate).format("YYYY-MM-DD hh:mm A")
    const text = `
Dear ${capitalizePatientName},
    
We are reminding you of your upcoming appointment with ${capitalizePartnerName} on ${appointmentTime}. 
    
If you have any questions or more enquiries, don’t hesitate to call Sonocare on  ${companyPhoneNumber} or send an email to ${companyMail}. Have a nice day!
    
Best,
    
Sonocare HealthCare`;
    const subject= `Reminder: Appointment with ${capitalizePartnerName} in 1 hour.`;
    const to = appointment.patientId.email;
    mail(subject, to, text, text)
}