import User from '../models/User';


/**
 * An Async function that Checks Whether a User Phone Exists
 * @param phone {String} - The User's phone
 * @retuns Boolean - True/False 
 */
export const phoneExist = async function phoneExist (phone) {
    console.log(phone);
    const user = await User.findOne({
        phone
    })
    .exec();

    if (user) {
        return true
    }

    return false;

}
