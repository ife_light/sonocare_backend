import User from '../models/User'; 

/**
 * Function to store user uploaded profile Pucture to Users Database
 * @param {String} id The User id
 * @param {String} imageLink Cloudinary Image Link
 * @param {String} publicId Cloudinary image public ID
 * @returns {Promise} The user info is returned
 */
export default async function (id, imageLink, publicId="") {
    const user = await User.updateOne({_id: id}, {
        $set: {
            profilePicUrl : imageLink,
            cloudinaryImagePublicId: publicId
        }
    }).exec()

    return user;
}