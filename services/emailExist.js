import User from '../models/User';


/**
 * An Async function that Checks Whether a User Email Exists
 * @param email {string} - The User's email
 * @retuns Boolean - True/False 
 */
export const emailExist = async function emailExist (email) {
    const user = await User.findOne({
        email
    })
    .exec();

    if (user) {
        return true
    }

    return false;

}
