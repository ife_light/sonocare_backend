/**
 * Code generator Function
 * It generates random numbers to be used in password confirmation
 * @param figures {Number} - For inserting the number of figures to generate
 * @returns Number - It returns a number
 */
export default function (figures) {
    let generatedCodeArray = [];
    for (let i = 0; i < figures; i++) {
        generatedCodeArray.push(Math.floor(Math.random() * 10))
    }

    let generatedCode = generatedCodeArray.join("");
    const generatedCodeInNumber = Number(generatedCode);
    return generatedCodeInNumber;
}